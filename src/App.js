import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import '../assets/scss/app.scss';
import './utils/icon.js'

// Redux persist
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import storage from 'redux-persist/lib/storage';
import storageSession from 'redux-persist/lib/storage/session'

//Reducers
import connexion from './components/redux/reducers/connexion';
import menu from './components/redux/reducers/menu';
import user from './components/redux/reducers/user';
import vehicule from './components/redux/reducers/vehicule';
import stats from './components/redux/reducers/stats';
import site from './components/redux/reducers/site';
import reservation from './components/redux/reducers/reservation';
import demandeLocation from './components/redux/reducers/demandeLocation';
import demandeUtilisateur from './components/redux/reducers/demandeUtilisateur';

//Composants
import Router from "./components/Router/Router";

// Config redux persist
const connexionPersistConfigAuth = {
    key: 'auth',
    storage: storageSession
};
const connexionPersistConfigMenu = {
    key: 'menu',
    storage,
};

const store  = createStore(
    combineReducers({
        auth: persistReducer(connexionPersistConfigAuth, connexion),
        menu: persistReducer(connexionPersistConfigMenu, menu),
        user,
        vehicule,
        stats,
        site,
        reservation,
        demandeLocation,
        demandeUtilisateur
    }),
    composeWithDevTools(applyMiddleware(thunk))
);

const reduxPersistor = persistStore(store);

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={reduxPersistor}>
                    <div className="App">
                        <Router/>
                    </div>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;
