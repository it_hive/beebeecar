import {connect} from "react-redux";
import Calendrier from './Calendrier.jsx';

import {getUserInfos} from "../../redux/reducers/connexion";
import {getReservations} from "../../redux/reducers/reservation";
import fetchReservation from "../../redux/actions/reservation";
import fetchVehicules from "../../redux/actions/vehicule";
import {getVehicule} from "../../redux/reducers/vehicule";

const mapStateToProps = (state, props) => {
    return {
        idOrganisme: getUserInfos(state).idOrganisme,
        listReservation: getReservations(state),
        listVehicule: getVehicule(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchReservation: (dateDebut, dateFin, idOrganisme) => dispatch(fetchReservation(dateDebut, dateFin, idOrganisme)),
        fetchVehicules: () => dispatch(fetchVehicules())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Calendrier);