import React, { Component } from 'react';
import {Segment, Button} from "semantic-ui-react";
import dateFns, { formatRelative } from "date-fns";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Evenement from './Evenements/Evenement';
import {formatDateServer} from '../../../utils/date';

// Bouchon
import Events from './Evenements';

var fr = require('date-fns/locale/fr'); // Import french translation for date

export default class Calendrier extends Component{

    constructor(props){
        super(props);
        this.state = {
            currentMonth: new Date(),
            currentDate: new Date(),
            selectedDate: new Date(),
            day: false,
            week: true,
            month: false
        }
        this.changeToToday = this.changeToToday.bind(this);
        this.prev = this.prev.bind(this);
        this.next = this.next.bind(this);
        this.changeToDay = this.changeToDay.bind(this);
        this.changeToWeek = this.changeToWeek.bind(this);
        this.changeToMonth = this.changeToMonth.bind(this);
    }

    getStartDate(){
        if(this.state.day === true){
            return this.state.currentDate;
        }else if(this.state.week === true){
            return dateFns.startOfWeek(this.state.currentDate, {weekStartsOn: 1});
        }else{
            return dateFns.startOfMonth(this.state.currentDate);
        }
    }

    getEndDate(){
        if(this.state.day === true){
            return this.state.currentDate;
        }else if(this.state.week === true){
            return dateFns.endOfWeek(this.state.currentDate, {weekStartsOn: 1});
        }else{
            return dateFns.endOfMonth(this.state.currentDate);
        }
    }

    componentWillMount() {
        this.props.fetchVehicules();
        this.props.fetchReservation(this.getStartDate(), this.getEndDate(), this.props.idOrganisme);
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if(nextState.currentDate !== this.state.currentDate){
            let startDate = null; let endDate = null;
            if(this.state.day === true){
                startDate = nextState.currentDate;
            }else if(this.state.week === true){
                startDate = dateFns.startOfWeek(nextState.currentDate, {weekStartsOn: 1});
            }else{
                startDate = dateFns.startOfMonth(nextState.currentDate);
            }

            if(this.state.day === true){
                endDate = nextState.currentDate;
            }else if(this.state.week === true){
                endDate = dateFns.endOfWeek(nextState.currentDate, {weekStartsOn: 1});
            }else{
                endDate = dateFns.endOfMonth(nextState.currentDate);
            }
            this.props.fetchReservation(startDate, endDate, this.props.idOrganisme);
            this.forceUpdate();
        }
    }

    renderHeader() {
        const dateFormat = "MMMM YYYY";
        return (
            <div>
                <div className="row flex-middle header">
                    <div className="col col-start">
                        <div className="group-button f-left">
                            <Button onClick={this.prev}><span className="icon"><FontAwesomeIcon icon="chevron-left"/></span></Button>
                            <Button onClick={this.changeToToday}>Today</Button>
                            <Button onClick={this.next}><span className="icon"><FontAwesomeIcon icon="chevron-right"/></span></Button>
                        </div>
                        <div className="title vertical-center">
                            <span>
                                {dateFns.format(this.state.currentDate, dateFormat, {locale: fr})}
                            </span>
                        </div>
                    </div>
                    <div className="col col-end group-button">
                        <Button className={ this.state.day === true ? 'active' : '' } onClick={this.changeToDay}>Jour</Button>
                        <Button className={ this.state.week === true ? 'active' : '' } onClick={this.changeToWeek}>Semaine</Button>
                    </div>
                </div>
            </div>
        );
    }

    renderDays() {
        const dateFormat = "dddd DD";
        const days = [];
        const numberOfDays = (this.state.day === true ? '1' : (this.state.week === true ? '7' : '30'));
        const className = (this.state.day === true ? 'days row day' : (this.state.week === true ? 'days row week' : 'days row'));

        for (let i = 0; i < numberOfDays; i++) {
            days.push(
                <div className="col cell col-center" key={`day-${i}`}>
                    {dateFns.format(dateFns.addDays(this.getStartDate(), i), dateFormat, {locale: fr})}
                </div>
            );
        }
        return <div className={className}><div className="col cell col-center" key="day"/>{days}</div>;
    }

    renderCells() {
        const dateFormat = "DD/MM/YYYY";
        const numberOfDays = (this.state.day === true ? '1' : (this.state.week === true ? '7' : '30'));
        const className = (this.state.day === true ? 'body day' : (this.state.week === true ? 'body week' : 'body cell'));
        const rows = [];

        this.props.listVehicule.map((item, i) => {
            const id = item.id_vehicule;
            const cells = [];

            for(let i = 0; i < numberOfDays; i++){
                cells.push(
                    <div data-date={dateFns.format(dateFns.addDays(this.getStartDate(), i), dateFormat)} className="col cell" key={`cell-${i}`}>
                        {this.renderEvents(dateFns.format(dateFns.addDays(this.getStartDate(), i), dateFormat), id)}
                    </div>
                );
            }

            rows.push(
                <div data-vehicule={item.id_vehicule} className="row">
                    <div className="col cell title">{item.immatriculation}</div>
                    {cells}
                </div>
            )
        });

        return <div className={className}>{rows}</div>;
    }

    renderEvents(date, idVehicule){
        const events = [];

        // Foreach on all events
        this.props.listReservation.map((item, i) => {
            const fin = item.dtdDate.split("/");
            const actual = date.split("/");
            const dateFin = new Date(dateFns.format(fin[2]+"-"+fin[1]+"-"+fin[0], "YYYY-MM-DD"));
            const actual_date = new Date(dateFns.format(actual[2]+"-"+actual[1]+"-"+actual[0], "YYYY-MM-DD"));
            if(dateFns.compareAsc(dateFin, actual_date) === 0 && item.vehicule.id_vehicule === idVehicule){
                events.push(
                    <Evenement
                        demande={item}
                        key={`${item.demandeLocation.demId}-${i}`}
                    />
                );
            }
        })

        return <div>{events}</div>;
    }

    formatDate(date){
        date = date.split("/");
        const d = new Date(date[2], date[1]-1, date[0]);
        return d;
    }

    next(){
        let newDate =
            (this.state.day === true ? dateFns.addDays(this.state.currentDate, 1) :
                    (this.state.week === true ? dateFns.addWeeks(this.state.currentDate, 1) : dateFns.addMonths(this.state.currentDate, 1))
            )
        this.setState({
            currentDate: newDate
        });
    }

    prev(){
        let newDate =
            (this.state.day === true ? dateFns.subDays(this.state.currentDate, 1) :
                    (this.state.week === true ? dateFns.subWeeks(this.state.currentDate, 1) : dateFns.subMonths(this.state.currentDate, 1))
            )
        this.setState({
            currentDate: newDate
        });
    }

    changeToToday(){
        this.setState({
            currentDate: new Date()
        });
    }

    changeToDay(){
        this.setState({
            day: true,
            week: false,
            month: false
        });
    }

    changeToWeek(){
        this.setState({
            day: false,
            week: true,
            month: false
        });
    }

    changeToMonth(){
        this.setState({
            day: false,
            week: false,
            month: true
        });
    }

    render(){
        return (
            <Segment className="fullPage">
                <div className="body">
                    <div className="calendar">
                        {this.renderHeader()}
                        {this.renderDays()}
                        {this.renderCells()}
                    </div>
                </div>
            </Segment>
        )
    }
}