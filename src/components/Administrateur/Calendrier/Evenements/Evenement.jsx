import React, { Component } from 'react';
import {Button, Modal} from "semantic-ui-react";
import AjouterVehicule from "../../Vehicule/Vehicule";
import DetailsDemande from "../../../User/DetailsDemande/DetailsDemande";
import {Td} from "react-super-responsive-table";

export default class Evenement extends Component{

    constructor(props){
        super(props);
        this.state = {
            openModal: false
        }
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal(){
        this.setState({
            openModal: true
        })
    }

    closeModal(){
        this.setState({
            openModal: false
        })
    }

    render(){
        const demande = this.props.demande;
        return (
            <Modal
                trigger={
                    <p style={{backgroundColor: demande.vehicule.couleur !== null  ? demande.vehicule.couleur : "#FFC038"}} className="event" onClick={this.openModal}>
                        { demande.utilisateur.nom.toUpperCase() } {demande.utilisateur.prenom}
                    </p>
                }
            >
            <Modal.Header>Détails de la demande</Modal.Header>
                <Modal.Content>
                    <Modal.Description>
                        <DetailsDemande demande={demande.demandeLocation} key={demande.demId}/>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}