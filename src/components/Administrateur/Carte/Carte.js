import {connect} from "react-redux";
import Map from './Carte.jsx';
import {setMenuChoice} from "../../redux/actions/menu";

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuChoice: (item) => dispatch(setMenuChoice(item))
    }
}

export default connect(null, mapDispatchToProps)(Map);