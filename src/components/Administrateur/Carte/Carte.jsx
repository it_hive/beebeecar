import React, { Component } from 'react';
import {
    LayersControl,
    Map,
    Marker,
    Popup,
    TileLayer,
} from 'react-leaflet';
import L from 'leaflet';
const { BaseLayer } = LayersControl;
import {Button} from "semantic-ui-react";

// Bouchon
import Vehicules from '../../../vehicules';
import { Redirect } from 'react-router-dom';

export const pointerIcon = new L.Icon({
    iconUrl: require('../../../../public/images/marker.png'),
    iconRetinaUrl: require('../../../../public/images/marker.png'),
    iconSize: [32, 32],
    shadowUrl: '../../../../public/images/marker.png',
})

export default class Carte extends Component{

    constructor(props){
        super(props);
        this.state = {
            lat: 47.214747,
            lng: -1.553116,
            zoom: 13,
            redirect: false
        };
        this.voirVehicule = this.voirVehicule.bind(this);
    }

    voirVehicule(){
        this.setState({
            redirect: true
        })
        this.props.setMenuChoice("car");
    }

    renderRedirect(){
        if(this.state.redirect){
            return <Redirect to="/car"/>
        }
    }

    generateMarkers(){
        const markers = [];

        for (let i = 0; i < Vehicules.length; i++){
            let position = [Vehicules[i].positionX, Vehicules[i].positionY];
            markers.push(
                <Marker position={position} icon={pointerIcon}>
                    <Popup>
                        <p>AN-219-VB</p>
                        <Button onClick={this.voirVehicule}>Voir le véhicule</Button>
                    </Popup>
                </Marker>
            )
        }

        return <div>{markers}</div>;
    }

    render(){
        const position = [this.state.lat, this.state.lng];
        return (
            <Map center={position} zoom={13}>
                <LayersControl position="topright">
                    <BaseLayer checked name="OpenStreetMap.Mapnik">
                        <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                    </BaseLayer>
                    <BaseLayer name="OpenStreetMap.BlackAndWhite">
                        <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                        />
                    </BaseLayer>
                </LayersControl>
                {this.generateMarkers()}
                {this.renderRedirect()}
            </Map>
        );
    }
}