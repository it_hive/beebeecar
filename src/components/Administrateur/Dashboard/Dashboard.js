import {connect} from "react-redux";
import Dashboard from './Dashboard.jsx';

import {setMenuChoice} from "../../redux/actions/menu";
import {fetchEtatParc, setActiveChart} from "../../redux/actions/stats";
import fetchUsers from "../../redux/actions/user";
import {getNBuser} from "../../redux/reducers/user";
import {getListVeh} from "../../redux/reducers/vehicule";
import {getListSiteOrga} from "../../redux/reducers/site";
import {fetchListSiteOrga} from "../../redux/actions/site";
import fetchVehicules from "../../redux/actions/vehicule";
import {getUserInfos, getUtilisateurToken} from "../../redux/reducers/connexion";
import {fetchDemandeLocation, fetchDemandesJour} from "../../redux/actions/demandeLocation";
import {getDemandeLocation, getDemandesJour} from "../../redux/reducers/demandeLocation";
import {getEtatParc} from "../../redux/reducers/stats";

const mapStateToProps = (state) => {
    return {
        getUtilisateurToken: getUtilisateurToken(state),
        nbUser : getNBuser(state),
        nbVeh : getListVeh(state).length,
        nbSite : getListSiteOrga(state).length,
        idOrganisme : getUserInfos(state).idOrganisme,
        demandeLocation : getDemandeLocation(state),
        getDemandesJour : getDemandesJour(state),
        getEtatParc : getEtatParc(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuChoice: (item) => dispatch(setMenuChoice(item)),
        setActiveChart: (item) => dispatch(setActiveChart(item)),
        fetchUsers: () => dispatch(fetchUsers()),
        fetchListSiteOrga: ()  => dispatch(fetchListSiteOrga()),
        fetchVehicules: () => dispatch(fetchVehicules()),
        fetchReservation: () => dispatch(fetchDemandeLocation()),
        fetchDemandeLocation: () => dispatch(fetchDemandeLocation()),
        fetchEtatParc: () => dispatch(fetchEtatParc()),
        fetchDemandesJour: () => dispatch(fetchDemandesJour())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);