import React, { Component, Fragment } from 'react';
import { Grid, Segment, Divider, Label, Table } from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {Link} from "react-router-dom";
import RevervationJour from './reservationJour/ReservationJour';
import DemandeAttente from "./DemandeAttente/DemandeAttente";
import JoyrideComponent from "../JoyrideComponent/JoyrideComponent.js";
import {setMenuChoice} from "../../redux/actions/menu";

export default class Dashboard extends Component{

    constructor(props){
        super(props);
        this.state = {
            runJoyride: false
        };
        this.onClickLink = this.onClickLink.bind(this);
        this.onChartClick = this.onChartClick.bind(this);
    }

    componentWillMount(){
        this.props.fetchUsers();
        this.props.fetchListSiteOrga(this.props.idOrganisme);
        this.props.fetchVehicules();
        this.props.fetchReservation();
        this.props.fetchDemandeLocation();
        this.props.fetchDemandesJour();
        this.props.fetchEtatParc();
    }

    componentDidMount() {
        this.props.setMenuChoice("dashboard");
        this.setState({
            runJoyride: true
        });
    }

    onClickLink(link){
        this.props.setMenuChoice(link);
    }

    onChartClick(chart){
        this.props.setMenuChoice("stats");
        this.props.setActiveChart(chart);
    }

    render(){
        return (
            <Fragment>
                <JoyrideComponent run={true}/>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4}>
                            <Segment raised className="widget widget_link_chart">
                                <div className="body">
                                    <Link to={'/stats'} onClick={() => this.onChartClick("nombre_reservation")} className="flex flex-row">
                                        <div className="flex-start">
                                            <div className="flex flex-column">
                                                <div>
                                                    <p className="widget_title">Nombre de réservation</p>
                                                </div>
                                                <div>
                                               <span className="widget_value">
                                                    <span className="big">{this.props.getDemandesJour.length}</span><span className="little">aujourd'hui</span>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex-end widget_icon">
                                            <FontAwesomeIcon icon={"chart-line"}/>
                                        </div>
                                    </Link>
                                </div>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Segment raised className="widget widget_link_chart">
                                <div className="body">
                                    <Link to={'/stats'} onClick={() => this.onChartClick("utilisateur_actif")} className="flex flex-row">
                                        <div className="flex-start">
                                            <div className="flex flex-column">
                                                <div>
                                                    <p className="widget_title">Utilisateurs actifs</p>
                                                </div>
                                                <div>
                                                <span className="widget_value">
                                                    <span className="big">2</span><span className="little"/>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex-end widget_icon">
                                            <FontAwesomeIcon icon={"user"}/>
                                        </div>
                                    </Link>
                                </div>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Segment raised className="widget widget_link_chart">
                                <div className="body">
                                    <Link to={'/stats'} onClick={() => this.onChartClick("etat_parc")} className="flex flex-row">
                                        <div className="flex-start">
                                            <div className="flex flex-column">
                                                <div>
                                                    <p className="widget_title">Véhicules disponibes</p>
                                                </div>
                                                <div>
                                                <span className="widget_value">
                                                    <span className="big">{this.props.getEtatParc.nombreVehiculeDisponible}</span><span className="little"/>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex-end widget_icon">
                                            <FontAwesomeIcon icon={"car-alt"}/>
                                        </div>
                                    </Link>
                                </div>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Segment raised className="widget widget_link_chart">
                                <div className="body">
                                    <Link to={'/stats'} onClick={() => this.onChartClick("taux_occupation")} className="flex flex-row">
                                        <div className="flex-start">
                                            <div className="flex flex-column">
                                                <div>
                                                    <p className="widget_title">Véhicules réservés</p>
                                                </div>
                                                <div>
                                                    <span className="widget_value">
                                                        <span className="big">{this.props.getEtatParc.nombreVehiculeIndisponible}</span><span className="little"/>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex-end widget_icon">
                                            <FontAwesomeIcon icon={"car-alt"}/>
                                        </div>
                                    </Link>
                                </div>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={12}>
                            <Segment className="widget widget_center">
                                <div className="header colored">
                                    <p className="title">
                                        Réservations aujourd'hui
                                    </p>
                                </div>
                                <div className="body">
                                    <RevervationJour />
                                </div>
                            </Segment>
                            <Segment className="widget widget_center">
                                <div className="header colored">
                                    <p className="title">
                                        {this.props.demandeLocation.length} Réservations en attente
                                    </p>
                                </div>
                                <div className="body">
                                    <DemandeAttente token={this.props.getUtilisateurToken.token}/>
                                </div>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <Segment className="widget widget_details">
                                <div className="header colored">
                                    <p className="title">
                                        Détails
                                    </p>
                                </div>
                                <div className="body">
                                    <Link to={'/user'} className="link" onClick={() => this.onClickLink("user")}>
                                        <Label circular>
                                            <FontAwesomeIcon icon={"user"}/>
                                        </Label>
                                        <span>{this.props.nbUser} collaborateurs</span>
                                    </Link>
                                    <Divider />
                                    <Link to={'/car'} className="link" onClick={() => this.onClickLink("car")}>
                                        <Label circular>
                                            <FontAwesomeIcon icon={"car"}/>
                                        </Label>
                                        <span>{this.props.nbVeh} véhicules</span>
                                    </Link>
                                    <Divider />
                                    <Link to={'/sites'} className="link" onClick={() => this.onClickLink("sites")}>
                                        <Label circular>
                                            <FontAwesomeIcon icon={"home"}/>
                                        </Label>
                                        <span>{this.props.nbSite} sites</span>
                                    </Link>
                                </div>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Fragment>
        );
    }
}