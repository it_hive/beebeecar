import {connect} from "react-redux";
import DemandeAttente from './DemandeAttente.jsx';
import {getDemandeLocation} from "../../../redux/reducers/demandeLocation";
import {fetchDemandeLocation} from "../../../redux/actions/demandeLocation";

const mapStateToProps = (state) => {
    return{
        demandeLocation : getDemandeLocation(state)
    }
};

export default connect(mapStateToProps, null)(DemandeAttente);