import React, { Component, Fragment } from 'react';
import { Table, Label } from 'semantic-ui-react'
import LigneDemandeAttente from "./LigneDemandeAttente";

export default class DemandeAttente extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <Fragment>
                {this.props.demandeLocation.length === 0  || !Array.isArray(this.props.demandeLocation) ? (
                    <p>Aucune demande en attente</p>
                ) : (
                    <Table basic='very' className="table table_center">
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Date de la demande</Table.HeaderCell>
                                <Table.HeaderCell>Collaborateur</Table.HeaderCell>
                                {/*<Table.HeaderCell>Véhicule</Table.HeaderCell>*/}
                                <Table.HeaderCell>Date de début</Table.HeaderCell>
                                <Table.HeaderCell>Date de Fin</Table.HeaderCell>
                                <Table.HeaderCell>Lieu de départ</Table.HeaderCell>
                                {/*<Table.HeaderCell/>*/}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {
                                this.props.demandeLocation.map((itemLimit, i) =>
                                    (i < 4 && (
                                        <LigneDemandeAttente demande={itemLimit} key={i} />
                                    ))
                                )
                            }
                        </Table.Body>
                    </Table>
                )}
            </Fragment>
        );
    }
}