import React, {Component, } from 'react';
import { Table, Label} from 'semantic-ui-react';
import dateFns, { formatRelative } from "date-fns";
import {formatDateServer} from '../../../../utils/date';
var fr = require('date-fns/locale/fr'); // Import french translation for date

let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class LigneDemandeAttente extends Component{

    constructor(props){
        super(props);
    }

    formateDate(date){
        let DateServer = formatDateServer(date);
        let formatDate = dateFns.format(DateServer, "DD/MM/YYYY", {locale: fr});
        return formatDate;
    }

    render(){
        const demande = this.props.demande;
        return (
                <Table.Row>
                    <Table.Cell>
                        {demande.dateDemande}
                    </Table.Cell>
                    <Table.Cell>
                        {demande.nomUtilisateur.toUpperCase()} {demande.prenomUtilisateur}
                    </Table.Cell>
                    <Table.Cell>
                        {demande.dateDebut}
                    </Table.Cell>
                    <Table.Cell>
                        {demande.dateFin}
                    </Table.Cell>
                    <Table.Cell>
                        {demande.nomSite}
                    </Table.Cell>
                </Table.Row>
        );
    }
}