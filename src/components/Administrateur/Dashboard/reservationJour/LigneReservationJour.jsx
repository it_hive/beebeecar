import React, {Component, Fragment} from 'react';
import { Grid, Table, Modal, Button } from 'semantic-ui-react'
import dateFns, { formatRelative } from "date-fns";
import { Link } from 'react-router-dom';


export default class LigneReservationJour extends Component{

    constructor(props){
        super(props);
    }

    render(){
        const reservation = this.props.reservation;
        return (
            <Fragment>
                <Table.Row>
                    <Table.Cell>{vehicule.immatriculation}</Table.Cell>
                    <Table.Cell>{vehicule.marque} {vehicule.modele}</Table.Cell>
                    <Table.Cell>{vehicule.kilometrage}</Table.Cell>
                    <Table.Cell>{vehicule.nomSite}</Table.Cell>
                    <Table.Cell>{dateFns.format(vehicule.dateMiseEnService, 'DD/MM/YYYY')}</Table.Cell>
                    <Table.Cell>
                        <Link to={{
                            pathname: '/car/details',
                            state: {
                                v: vehicule
                            }
                        }}>
                            <Button>Détails</Button>
                        </Link>
                    </Table.Cell>
                </Table.Row>
            </Fragment>
        );
    }
}