import {connect} from "react-redux";
import ReservationJour from './ReservationJour.jsx';
import {getUserInfos} from "../../../redux/reducers/connexion";
import {getDemandesJour} from "../../../redux/reducers/demandeLocation";
import {setMenuChoice} from "../../../redux/actions/menu";
import {setActiveChart} from "../../../redux/actions/stats";
import fetchUsers from "../../../redux/actions/user";
import {fetchListSiteOrga} from "../../../redux/actions/site";
import fetchVehicules from "../../../redux/actions/vehicule";
import {fetchDemandeLocation, fetchDemandesJour} from "../../../redux/actions/demandeLocation";

const mapStateToProps = (state, props) => {
    return {
        idOrganisme: getUserInfos(state).idOrganisme,
        getDemandesJour : getDemandesJour(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDemandesJour: () => dispatch(fetchDemandesJour())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ReservationJour);