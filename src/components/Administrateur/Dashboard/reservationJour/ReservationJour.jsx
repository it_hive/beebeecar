import React, { Component, Fragment } from 'react';
import { Table, Label } from 'semantic-ui-react'

export default class ReservationJour extends Component{
    componentWillMount() {
        this.props.fetchDemandesJour();
    }

    render(){
        return (
            <Fragment>
                {this.props.getDemandesJour.length === 0  || !Array.isArray(this.props.getDemandesJour) ? (
                    <p>Aucune réservation aujourd'hui</p>
                ) : (
                    <Table basic='very' className="table table_center">
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Collaborateur</Table.HeaderCell>
                                <Table.HeaderCell>Véhicule</Table.HeaderCell>
                                <Table.HeaderCell>Date de début</Table.HeaderCell>
                                <Table.HeaderCell>Date de Fin</Table.HeaderCell>
                                <Table.HeaderCell>Lieu de départ</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {this.props.getDemandesJour !== undefined && (this.props.getDemandesJour.map((item, i) => {
                                return (
                                    <Table.Row key={i}>
                                        <Table.Cell>
                                            {item.nomUtilisateur.toUpperCase()} {item.prenomUtilisateur}
                                        </Table.Cell>
                                        <Table.Cell>
                                            ABS 123 DE
                                        </Table.Cell>
                                        <Table.Cell>
                                            {item.dateDebut}
                                        </Table.Cell>
                                        <Table.Cell>
                                            {item.dateFin}
                                        </Table.Cell>
                                        <Table.Cell>
                                            Nantes
                                        </Table.Cell>
                                    </Table.Row>
                                )
                            }))}
                        </Table.Body>
                    </Table>
                )}
            </Fragment>
        );
    }
}