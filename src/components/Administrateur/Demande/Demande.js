import {connect} from "react-redux";
import Demande from './Demande.jsx';
import {fetchDemandeLocation, setDemandeRefuse, setDemandeValidate} from "../../redux/actions/demandeLocation";
import {demandeIsRefuse, demandeIsValidate, getDemandeLocation} from "../../redux/reducers/demandeLocation";

const mapStateToProps = (state) => {
    return{
        demandeLocation : getDemandeLocation(state),
        demandeIsValidate : demandeIsValidate(state),
        demandeIsRefuse : demandeIsRefuse(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return{
        fetchDemandeLocation: () => dispatch(fetchDemandeLocation()),
        setDemandeValidate: (donnee) => dispatch(setDemandeValidate(donnee)),
        setDemandeRefuse: (donnee) => dispatch(setDemandeRefuse(donnee))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Demande);