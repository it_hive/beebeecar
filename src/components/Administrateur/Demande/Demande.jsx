import React, {Component, Fragment} from 'react';
import {Button, Grid, Message, Modal, Segment, Table} from "semantic-ui-react";
import Filter from "../Filter/Filter";
import DemandeLigne from "./DemandeLigne/DemandeLigne";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../translate/fr.yml'));

export default class Demande extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: null,
            message: true
        };
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    open(){
        this.setState({ open: true })
    }
    close(){
        this.setState({ open: false })
    }

    componentWillMount(){
        this.props.fetchDemandeLocation();
    }

    componentDidUpdate(){
        if((this.props.demandeIsValidate === 200 || this.props.demandeIsRefuse === 200) && (this.state.open)){
            this.setState({ open: false, message: false});
            setTimeout(() => {
                this.setState({ message: true, open: null });
                this.props.setDemandeValidate(null);
                this.props.setDemandeRefuse(null);
            }, 3000);
        }
    }

    render(){
        return (
            <Fragment>
                <Message
                    positive
                    hidden = {this.state.message}
                    header={this.props.demandeIsValidate == 200?T.translate("app.demande.validationDemande"):T.translate("app.demande.refusDemande")}
                    content={this.props.demandeIsValidate == 200?T.translate("app.demande.validationDemandeLong"):T.translate("app.demande.refusDemandeLong")}
                />
                <Segment>
                    <div className="header colored">
                        <p className="title">
                            Liste des demandes en attente
                        </p>
                    </div>
                    <div className="body">
                        {this.props.demandeLocation.length !== 0 ? (
                            <Table basic='very' className="table table_center">
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>{T.translate("app.reservation.numeroReservation")}</Table.HeaderCell>
                                        <Table.HeaderCell>{T.translate("app.reservation.demandeur")}</Table.HeaderCell>
                                        <Table.HeaderCell>{T.translate("app.reservation.dateDemande")}</Table.HeaderCell>
                                        <Table.HeaderCell>{T.translate("app.reservation.dateDebut")}</Table.HeaderCell>
                                        <Table.HeaderCell>{T.translate("app.reservation.dateFin")}</Table.HeaderCell>
                                        <Table.HeaderCell>{T.translate("app.reservation.voiture")}</Table.HeaderCell>
                                        <Table.HeaderCell>{T.translate("app.reservation.covoiturage")}</Table.HeaderCell>
                                        <Table.HeaderCell/>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.props.demandeLocation.map((item, i) =>
                                        <DemandeLigne
                                            demande={item}
                                            key={i}
                                            onOpen={this.open}
                                            onClose={this.close}/>
                                    )}
                                </Table.Body>
                            </Table>
                        ) : (
                            <p>Aucune demande en attente.</p>
                        )}
                    </div>
                </Segment>
            </Fragment>
        );
    }
}