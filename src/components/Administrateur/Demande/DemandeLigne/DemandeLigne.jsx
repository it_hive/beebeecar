import React, {Component} from 'react';
import {Button, Modal, Table} from "semantic-ui-react";
import DetailsReservation from "./../DetailsReservation";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class DemandeLigne extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: false
        };
        this.formatDate = this.formatDate.bind(this);
    }

    formatDate(date){
        date = date.split("-");
        return date[2].substr(0, 2) + '/' + date[1] + '/' + date[0];
    }

    render(){
        const demande = this.props.demande;

        return (
            <Table.Row>
                <Table.Cell>N° {demande.demId} </Table.Cell>
                <Table.Cell>{demande.nomUtilisateur.toUpperCase()} {demande.prenomUtilisateur}</Table.Cell>
                <Table.Cell>{demande.dateDemande}</Table.Cell>
                <Table.Cell>{demande.dateDebut}</Table.Cell>
                <Table.Cell>{demande.dateDebut}</Table.Cell>
                <Table.Cell>Clio</Table.Cell>
                <Table.Cell>{demande.allowCovoiturage?T.translate("app.global.yes"):T.translate("app.global.no")}</Table.Cell>
                <Table.Cell>
                    <Modal
                        trigger={<Button key={demande.demId}>{T.translate("app.reservation.details")}</Button>}
                        onOpen={this.props.onOpen}
                        onClose={this.props.onClose}
                        key={demande.demId}
                    >
                        <Modal.Header>Détails de la demande</Modal.Header>
                        <Modal.Content>
                            <Modal.Description>
                                <DetailsReservation idDemande={demande.demId} demande={demande} key={demande.demId}/>
                            </Modal.Description>
                        </Modal.Content>
                    </Modal>
                </Table.Cell>
            </Table.Row>
        );
    }
}