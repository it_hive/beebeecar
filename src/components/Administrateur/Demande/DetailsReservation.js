import {connect} from "react-redux";
import DetailsReservation from './DetailsReservation.jsx';
import {getDemandeLocationDetails} from "../../redux/reducers/demandeLocation";
import {
    fetchDemandeLocationDetails,
    refuserDemandeLocation,
    validerDemandeLocation
} from "../../redux/actions/demandeLocation";
import {getDemandeVehiculeDispo} from "../../redux/reducers/demandeUtilisateur";
import {fetchDemandeVehiculeDispo} from "../../redux/actions/demandeUtilisateur";

const mapStateToProps = (state) => {
    return{
        getDemandeLocationDetails : getDemandeLocationDetails(state),
        getDemandeVehiculeDispo: getDemandeVehiculeDispo(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return{
        fetchDemandeLocationDetails: (id) => dispatch(fetchDemandeLocationDetails(id)),
        validerDemandeLocation: (idUtilisateur, idDemande, idVehicule) => dispatch(validerDemandeLocation(idUtilisateur, idDemande, idVehicule)),
        refuserDemandeLocation: (idUtilisateur, idDemande) => dispatch(refuserDemandeLocation(idUtilisateur, idDemande)),
        fetchDemandeVehiculeDispo: (idOrga, dateDebut, dateFin) => dispatch(fetchDemandeVehiculeDispo(idOrga, dateDebut, dateFin)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailsReservation);