import React, { Component, Fragment } from 'react';
import {Grid, Button, Form, Comment, Header, Table} from "semantic-ui-react";
import UserAvatar from "../../../../public/images/user-avatar.svg";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../translate/fr.yml'));

export default class DetailsReservation extends Component{

    constructor(props){
        super(props);
        this.state = {
            voiture: null,
            errorVoiture: false
        };
        this.validerDemande = this.validerDemande.bind(this);
    }

    componentDidMount() {
        if(this.props.demande.vehicule === null){
            const debut = this.props.demande.dateDebut.split('/');
            const fin = this.props.demande.dateFin.split('/');
            this.props.fetchDemandeVehiculeDispo(this.props.demande.idSite, debut[2]+"-"+debut[1]+"-"+debut[0], fin[2]+"-"+fin[1]+"-"+fin[0]);
        }
    }

    optionVehicule(){
        let option = [];
        const listSite = this.props.getDemandeVehiculeDispo;
        listSite && listSite.forEach(item => {
            option.push({
                key: item.id_vehicule,
                text: item.marque+" "+item.modele,
                value: item.id_vehicule
            })
        });
        return option;
    }

    validerDemande() {
        const demande = this.props.demande;

        if (this.props.demande.vehicule === null && this.state.voiture === null) {
            this.setState({errorVoiture: true})
        }else{
            if(demande.vehicule === null){
                this.props.validerDemandeLocation(demande.idUtilisateur, demande.demId, this.state.voiture)
            }else{
                this.props.validerDemandeLocation(demande.idUtilisateur, demande.demId, demande.vehicule.id_vehicule)
            }
        }
    }

    generateEtape(demande){
        const ol_etapes = [];

        function compare(a, b){
            // Use toUpperCase() to ignore character casing
            const genreA = a.ordre;
            const genreB = b.ordre;

            let comparison = 0;
            if (genreA > genreB) {
                comparison = 1;
            } else if (genreA < genreB) {
                comparison = -1;
            }
            return comparison;
        }

        if(demande.etapes !== null && demande.etapes.length !== 0){
            const etapes = demande.etapes.sort(compare);
            etapes.map((item, i) => {
                ol_etapes.push(
                    <li key={i} style={{paddingLeft: '5px'}}>{item.adresse}</li>
                )
            });

            return <ol style={{marginTop: '0', paddingLeft: '25px'}}>{ol_etapes}</ol>;
        }

        return null
    }

    render(){
        const demande = this.props.demande;
        return(
            <Grid>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <p><strong>Numéro :</strong> {demande.demId}</p>
                        <p><strong>Date de la demande :</strong> {demande.dateDemande}</p>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <p><strong>Demandeur :</strong> {demande.nomUtilisateur.toUpperCase()} {demande.prenomUtilisateur}</p>
                        <p><strong>Date de début :</strong> {demande.dateDebut}</p>
                        <p><strong>Date de fin :</strong> {demande.dateFin}</p>
                        {(demande.commentaire !== null && demande.commentaire !== "") && (
                            <p><strong>Commentaire :</strong> {demande.commentaire}</p>
                        )}
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <p><strong>Lieu de départ :</strong> {demande.nomSite} </p>
                        {demande.vehicule !== null ?
                            (
                                <p><strong>Voiture : </strong> {demande.vehicule.marque+" "+demande.vehicule.modele+" - "+demande.vehicule.immatriculation}</p>
                            ) : (
                                <div>
                                    <Form>
                                        <Form.Field>
                                            <Form.Select
                                                className="select"
                                                label="Voiture"
                                                options={this.optionVehicule()}
                                                onChange={ (event, {value}) => {
                                                    this.setState({voiture: value});
                                                    if(this.state.errorVoiture === true){
                                                        this.setState({errorVoiture: !this.state.errorVoiture})
                                                    }
                                                }}
                                                error={this.state.errorVoiture}
                                                value={this.state.voiture}
                                            />
                                        </Form.Field>
                                    </Form>
                                </div>
                            )
                        }
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <p><strong>Autorise le covoiturage :</strong> {demande.allowCovoiturage?T.translate("app.global.yes"):T.translate("app.global.no")}</p>
                        {demande.etapes.length !== 0 && (
                            <Fragment>
                                <p style={{marginBottom: '5px'}}><strong>Etapes :</strong></p>
                                {this.generateEtape(demande)}
                            </Fragment>
                        )}
                        {(demande.commentaireCovoit !== null && demande.commentaireCovoit !== "") && (
                            <p><strong>Commentaire :</strong> {demande.commentaireCovoit}</p>
                        )}
                    </Grid.Column>
                </Grid.Row>
                {demande.canValidate && (
                    <Grid.Row>
                        <div className="flex flex-row">
                            <div className="flex-center">
                                <Button color='red' onClick={() => {this.props.refuserDemandeLocation(demande.idUtilisateur, demande.demId)}}>Refuser</Button>
                                <Button onClick={() => {this.validerDemande()}}>Valider</Button>
                            </div>
                        </div>
                    </Grid.Row>
                )}
            </Grid>
        )
    }
}