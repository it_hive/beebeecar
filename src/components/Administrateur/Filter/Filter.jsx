import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome/index'

export default class Filter extends Component {

    constructor(props){
        super(props);
        this.state = {
            active: false
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){
        this.setState({active: !this.state.active});
    }

    render() {
        const name = this.state.active ? 'angle-down' : 'angle-up';
        return (
            <FontAwesomeIcon icon={name} onClick={this.handleClick}/>
        );
    }
}
