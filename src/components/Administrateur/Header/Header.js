import {connect} from "react-redux";
import Header from './Header.jsx';
import {disconnect} from "../../redux/actions/connexion";
import {getFirstAndUsername} from "../../redux/reducers/connexion";
import {setOpenMenuDashboard} from "../../redux/actions/menu";
import {getOpenMenuDashboard} from "../../redux/reducers/menu";

const mapStateToProps = (state) => {
    return {
        username : getFirstAndUsername(state),
        isMenuOpen: getOpenMenuDashboard(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deconnexion: () => dispatch(disconnect()),
        setOpenMenu: (item) => dispatch(setOpenMenuDashboard(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
