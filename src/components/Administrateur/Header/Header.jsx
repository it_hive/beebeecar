import React, {Component} from 'react';
import {Dropdown, Image } from 'semantic-ui-react';
import Icon from '../../../../public/images/user-avatar.svg';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Link, Redirect} from "react-router-dom";

export default class Header extends Component {

    constructor(props){
        super(props);
        this.state = {
            redirect: false
        }
        this.deconnexion = this.deconnexion.bind(this);
        this.showProfile = this.showProfile.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    deconnexion(){
        this.props.deconnexion()
    }

    showProfile(){
        this.setState({
            redirect: true
        })
    }

    redirectToProfile(){
        if(this.state.redirect){
            this.setState({redirect: false});
            return <Redirect to="/profile"/>
        }
    }

    closeMenu(){
        if(this.props.isMenuOpen === true){
            this.props.setOpenMenu(false);
        }else{
            this.props.setOpenMenu(true);
        }
    }

    render() {
        return(
            <div className="flex headerAdmin">
                <div className="hamburger-icon flex-start">
                    <FontAwesomeIcon icon="bars" className="icon" onClick={this.closeMenu}/>
                </div>
                <div className="flex-end">
                    <div className="user">
                        {/*<div className="header_item">*/}
                        {/*    <Link to={'/profile'} className="link">*/}
                        {/*        <FontAwesomeIcon icon="user-circle"/>*/}
                        {/*        <span>Mon compte</span>*/}
                        {/*    </Link>*/}
                        {/*</div>*/}
                        {/*<div className="header_item" onClick={this.deconnexion}>*/}
                        {/*    <FontAwesomeIcon icon="sign-out-alt"/>*/}
                        {/*    <span>Déconnexion</span>*/}
                        {/*</div>*/}

                        {/*<div className="headerIcon">*/}
                        {/*    <Image src={Icon} size='mini' circular />*/}
                        {/*    /!*<FontAwesomeIcon icon="user-circle" size="1x"/>*!/*/}
                        {/*</div>*/}
                        <Dropdown text={this.props.username}>
                            <Dropdown.Menu>
                                <Dropdown.Item text="Mon compte" onClick={this.showProfile}/>
                                <Dropdown.Item text="Deconnexion" onClick={this.deconnexion}/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </div>
                {this.redirectToProfile()}
            </div>
        );
    }
}