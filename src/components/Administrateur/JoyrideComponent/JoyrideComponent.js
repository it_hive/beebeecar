import {connect} from "react-redux";
import JoyrideComponent from "./JoyrideComponent.jsx";
import {setShowTuto} from "../../redux/actions/user";
import {getShowTuto} from "../../redux/reducers/user";

const mapStateToProps = (state) => {
    return {
        showTuto: getShowTuto(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setShowTuto: (showTuto) => dispatch(setShowTuto(showTuto))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(JoyrideComponent);