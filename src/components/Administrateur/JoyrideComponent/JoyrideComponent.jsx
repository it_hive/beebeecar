import React, {Component, Fragment} from "react";
import Joyride from 'react-joyride';
import Tooltip from "./Tooltip/Tooltip";
import {Checkbox} from "semantic-ui-react";
import {getShowTuto} from "../../redux/reducers/user";

export default class JoyrideComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            show: true,
            steps: [
                {
                    content: (
                        <Fragment>
                            <p>Gestion des sites de l'organisation</p>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_site',
                    title: 'Sites',
                    disableBeacon: true
                },
                {
                    content: (
                        <Fragment>
                            <p>Gestion des véhicules</p>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_car',
                    title: 'Véhicules',
                    disableBeacon: true
                },
                {
                    content: (
                        <Fragment>
                            <p>Gestion des collaborateurs</p>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_user',
                    title: 'Collaborateurs',
                    disableBeacon: true
                },
                {
                    content: (
                        <Fragment>
                            <p>Données statistiques sur votre parc automobile</p>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_stat',
                    title: 'Statistiques',
                    disableBeacon: true
                },
                {
                    content: (
                        <Fragment>
                            <p>Calendrier des réservations</p>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_calendrier',
                    title: 'Réservations',
                    disableBeacon: true
                },
                {
                    content: (
                        <Fragment>
                            <p>Liste des demandes en attente</p>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_demande_attente',
                    title: 'Liste demandes',
                    disableBeacon: true
                },
                {
                    content: (
                        <Fragment>
                            <p>Écran d'accueil de l'interface d'administration</p>
                            <div className="stop_show">
                                <Checkbox
                                    label="Ne plus afficher"
                                    onChange={() => this.handleShowTuto()}
                                    value={this.props.showTuto}
                                />
                            </div>
                        </Fragment>
                    ),
                    placement: 'left-start',
                    target: '.link_dashboard',
                    title: 'Dashboard',
                    disableBeacon: true
                }
            ]
        };
        this.handleShowTuto = this.handleShowTuto.bind(this);
        this.handleJoyrideCallback = this.handleJoyrideCallback.bind(this);
    }

    handleShowTuto(){
        this.props.setShowTuto(false);
    }

    handleJoyrideCallback(data){
        console.log(data);
        if(data.action === 'close'){
            this.props.setShowTuto(false);
        }
    }

    render(){
        return(
            <Joyride
                callback={this.handleJoyrideCallback}
                run={this.props.showTuto}
                continuous={true}
                showSkipButton={true}
                steps={this.state.steps}
                tooltipComponent={Tooltip}
                disableCloseOnEsc={false}
                disableOverlayClose={false}
                styles={{
                    options: {
                        zIndex: 10000,
                    },
                }}
            />
        )
    }
}