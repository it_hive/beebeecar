import React from "react";
import Joyride, { BeaconRenderProps, TooltipRenderProps } from 'react-joyride';
import {Button, Checkbox} from "semantic-ui-react";

const Tooltip = (initialProps) => (
    <div {...initialProps.tooltipProps} className="tooltip_body">
        {initialProps.step.title && <h2 className="tooltip_title">{initialProps.step.title}</h2>}
        <div className="tooltip_content">{initialProps.step.content}</div>
        <div className="tooltip_footer">
            <div className="div_btn">
                <div className="btn_start">
                    {initialProps.index > 0 && (
                        <Button {...initialProps.backProps} className="btn_previous">
                            Précédent
                        </Button>
                    )}
                </div>
                <div className="btn_end">
                    {(initialProps.index + 1 !== initialProps.size) && initialProps.continuous && (
                        <Button {...initialProps.primaryProps} className="btn_next">
                            Suivant
                        </Button>
                    )}
                    {/*{(initialProps.index + 1 === initialProps.size) && initialProps.continuous && (*/}
                        <Button {...initialProps.closeProps} className="btn_close" >
                            Fermer
                        </Button>
                    {/*)}*/}
                </div>
            </div>
        </div>
    </div>
);

export default Tooltip;