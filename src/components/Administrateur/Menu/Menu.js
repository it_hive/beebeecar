import {connect} from "react-redux";
import Menu from './Menu.jsx';

import {getMenuChoice} from "./../../redux/reducers/menu";

const mapStateToProps = (state) => {
    return {
        getMenuChoice : getMenuChoice(state)
    }
}

export default connect(mapStateToProps)(Menu);