import React, { Component } from 'react';
import Logo from '../../../../public/images/Logo.png';
import LogoCloseMenu from '../../../../public/images/LogoVide.png';
import MenuButton from './MenuButton/MenuButton';

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../translate/fr.yml'));

export default class Menu extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return <div className="dashboardMenu">
            <div className="logo">
                <img src={Logo} className="default_logo" alt="BeeBeeCar"/>
                <img src={LogoCloseMenu} className="little_logo" alt="BeeBeeCar"/>
            </div>
            <MenuButton
                icon="tachometer-alt"
                label={T.translate("app.menu.dashboard")}
                link=''
                active={"dashboard" === this.props.getMenuChoice}
                class="link_dashboard"
            />
            <MenuButton
                icon="calendar-check"
                label={T.translate("app.menu.demande")}
                link='demande'
                active={"demande" === this.props.getMenuChoice}
                class="link_demande_attente"
            />
            <MenuButton
                icon="calendar"
                label={T.translate("app.menu.calendar")}
                link='calendar'
                active={"calendar" === this.props.getMenuChoice}
                class="link_calendrier"/>
            {/*<MenuButton
                icon="globe-europe"
                link='map'
                label={T.translate("app.menu.map")}
                active={"map" === this.props.getMenuChoice}
                class="link_map"/>*/}
            <MenuButton
                icon="chart-bar"
                link='stats'
                label={T.translate("app.menu.chart")}
                active={"stats" === this.props.getMenuChoice}
                class="link_stat"/>
            <MenuButton
                icon="car"
                link='car'
                label={T.translate("app.menu.car")}
                active={"car" === this.props.getMenuChoice}
                class="link_car"/>
            <MenuButton
                icon="home"
                link='sites'
                label={T.translate("app.menu.place")}
                active={"sites" === this.props.getMenuChoice}
                class="link_site"/>
            <MenuButton
                icon="user"
                link='user'
                label={T.translate("app.menu.user")}
                active={"user" === this.props.getMenuChoice}
                class="link_user"/>
            {/*<MenuButton*/}
            {/*    icon="cog"*/}
            {/*    link='setting'*/}
            {/*    label={T.translate("app.menu.setting")}*/}
            {/*    active={"setting" === this.props.getMenuChoice}/>*/}
        </div>;
    }
}