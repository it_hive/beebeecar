import {connect} from "react-redux";
import MenuButton from './MenuButton.jsx';

import {setMenuChoice} from "../../../redux/actions/menu";

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuChoice: (item) => dispatch(setMenuChoice(item))
    }
}

export default connect(null, mapDispatchToProps)(MenuButton);