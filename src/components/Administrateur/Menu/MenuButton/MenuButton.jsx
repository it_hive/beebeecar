import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

export default class menuButton extends Component{

    constructor(props){
        super(props);

        this.onClickAction = this.onClickAction.bind(this);
    }

    onClickAction(){
        const link = (this.props.link !== "") ? this.props.link : "dashboard";
        this.props.setMenuChoice(link);
    }

    render(){
        const className = this.props.active ? `userMenuButton active ${this.props.class}` : `userMenuButton ${this.props.class}`;
        return (
            <Link to={'/' + this.props.link} className="link">
                <div className={className} onClick={this.onClickAction}>
                    <span className="icon"><FontAwesomeIcon icon={this.props.icon}/></span>
                    <span className="title">{this.props.label}</span>
                </div>
            </Link>
        );
    }
}