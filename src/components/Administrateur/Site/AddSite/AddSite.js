import {connect} from "react-redux";
import AddSite from './AddSite.jsx';
import {saveSite, updateSite} from "../../../redux/actions/site";

const mapDispatchToProps = (dispatch) => {
    return {
        saveSite: (input) => dispatch(saveSite(input)),
        updateSite: (input, idSite) => dispatch(updateSite(input, idSite))
    }
}

export default connect(undefined, mapDispatchToProps)(AddSite);