import React, { Component } from 'react';
import { Segment, Button, Checkbox, Form, Grid, Input, Select, Header } from 'semantic-ui-react'

let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class AddSite extends Component{

    constructor(props){
        super(props);
        this.state = {
            input: {},
            defaultNom: null,
            defaultAdresse: null,
            defaultVille: null,
            defaultCP: null
        };
        this.onClickAction = this.onClickAction.bind(this);
    }

    onClickAction(){
        if(this.props.defaultSite){
            this.props.updateSite(this.state.input, this.props.defaultSite.sitId);
            this.props.onClose();
        }
        else
            this.props.saveSite(this.state.input);
    }
    componentWillMount(){
        if(this.props.defaultSite){
            let site = this.props.defaultSite
            const input = {
                adresse: site.adresse,
                codePostal: site.codePostal,
                nom: site.nom,
                phone: site.telephone,
                ville: site.ville};
            this.setState({
                defaultNom: site.nom,
                defaultVille: site.ville,
                defaultAdresse: site.adresse,
                defaultCP: site.codePostal,
                input
            })
        }
    }

    render(){
        return (
            <Form>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4} />
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.site.nom")}
                                    type="text"
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "name" : value}})}
                                    defaultValue={this.state.defaultNom}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.site.ville")}
                                    type="text"
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "city" : value}})}
                                    defaultValue={this.state.defaultVille}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.site.adresse")}
                                    type="text"
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "adress" : value}})}
                                    defaultValue={this.state.defaultAdresse}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.site.codePostal")}
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "codePostal" : value}})}
                                    type="text"
                                    defaultValue={this.state.defaultCP}
                                    required
                                />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={4} />
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={6} />
                        <Grid.Column width={4}>
                            <Button
                                fluid
                                type='submit'
                                onClick={this.onClickAction}
                            >
                                {this.props.defaultSite?T.translate("app.site.edit"):T.translate("app.site.add")}
                            </Button>
                        </Grid.Column>
                        <Grid.Column width={6} />
                    </Grid.Row>
                </Grid>
            </Form>
        );
    }
}

