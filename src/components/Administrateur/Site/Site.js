import {connect} from "react-redux";
import Site from './Site.jsx';
import {getIsCreate} from "../../redux/reducers/site";
import {setNewSite} from "../../redux/actions/site";

const mapStateToProps = (state) => {
    return {
        isCreate : getIsCreate(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setNewSite : (donnee) => dispatch(setNewSite(donnee))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Site);