import React, {Component, Fragment} from 'react';
import { Segment, Button, Grid, Modal, Message } from 'semantic-ui-react'

import TableauSite from "./TableauSite/TableauSite";
import AddSite from "./AddSite/AddSite";

let T = require('i18n-react').default;
T.setTexts(require("json-loader!yaml-loader!./../../../translate/fr.yml"));

export default class Site extends Component{

    constructor(props){
        super(props);
        this.state = {
            show: false,
            open: null,
            message: true
        };
        this.handleShow = this.handleShow.bind(this);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    handleShow(e){
        this.setState({show: !this.state.show});
    }

    open(){
        this.setState({ open: true });
    }
    close(){
        this.setState({ open: false });
    }

    componentDidUpdate(){
        if((this.props.isCreate === 200 || this.props.isArchive === 200) && (this.state.open || this.state.openArchive)){
            this.props.setNewSite(null);
            this.setState({ open: false, message: false });
            setTimeout(() => {
                this.setState({ message: true });
            }, 3000);
        }
    }

    render(){
        return (
            <Fragment>
                <Message
                    positive
                    hidden = {this.state.message}
                    onDismiss={this.handleDismiss}
                    header={T.translate("app.site.isCreate")}
                    content={T.translate("app.site.isCreateLong")}
                />
                <Segment>
                    <div className="header colored">
                        <div className="flex flex-row">
                            <p className="title flex-start">
                                {T.translate("app.site.listMessage")}
                            </p>
                            <div className="flex-end">
                                <Modal open={this.state.open}
                                       onOpen={this.open}
                                       onClose={this.close}
                                       trigger={<Button className="addVeh">{T.translate("app.site.addTitle")}</Button>}>
                                    <Modal.Header>{T.translate("app.site.addTitle")}</Modal.Header>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <AddSite/>
                                        </Modal.Description>
                                    </Modal.Content>
                                </Modal>
                            </div>
                        </div>
                    </div>
                    <div className="body">
                        <Grid>
                            <Grid.Row>
                                <Grid.Column>
                                    <TableauSite show={this.state.show}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                </Segment>
            </Fragment>
        );
    }
}

