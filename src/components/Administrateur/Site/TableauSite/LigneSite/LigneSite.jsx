import React, { Component } from 'react';
import { Grid, Table, Modal, Button } from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import dateFns, { formatRelative } from "date-fns";
import AddUser from "../../../User/AddUser/AddUser";
import AddSite from "../../AddSite/AddSite";
let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../../translate/fr.yml'));

export default class LigneSite extends Component{

    constructor(props){
        super(props);
        this.state = {
            openEdit: false,
            open: false,
            idSite: null
        };
        this.close = this.close.bind(this);
        this.closeEdit = this.closeEdit.bind(this);
        this.confirmEdit = this.confirmEdit.bind(this);
    }

    archive(){
        this.props.archiveSite(this.state.idSite);
    }

    closeEdit(){
        this.setState({openEdit: false})
    }

    confirmEdit(){
        this.setState({openEdit: true})
    }

    close(){
        this.setState({open: false});
    }

    render(){
        const site = this.props.site;
        return (
            <Table.Row>
                <Table.Cell>{site.nom}</Table.Cell>
                <Table.Cell>{site.ville}</Table.Cell>
                <Table.Cell>{site.adresse}</Table.Cell>
                <Table.Cell>{site.codePostal}</Table.Cell>
                <Table.Cell>
                    <Modal
                        open={this.state.openEdit}
                        onClose={this.closeEdit}
                        trigger={
                        <Button className="icon-before" onClick={this.confirmEdit}>
                            <FontAwesomeIcon icon="pencil-alt"/> Modifier
                        </Button>
                    }>
                        <Modal.Header>{T.translate("app.site.editTitle")}</Modal.Header>
                        <Modal.Content>
                            <Modal.Description>
                                <AddSite defaultSite={site} onClose={this.closeEdit}/>
                            </Modal.Description>
                        </Modal.Content>
                    </Modal>
                </Table.Cell>
            </Table.Row>
        );
    }
}