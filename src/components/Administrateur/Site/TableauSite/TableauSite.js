import {connect} from "react-redux";
import TableauSite from './TableauSite.jsx';
import {fetchListSiteOrga} from "../../../redux/actions/site"
import {getUserInfos} from "../../../redux/reducers/connexion";
import {getListSiteOrga} from "../../../redux/reducers/site"

const mapStateToProps = (state, ownProps) => {
    return({
        idOrganisme: getUserInfos(state).idOrganisme,
        getListSite: getListSiteOrga(state)
    });
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchListSiteOrga: () => dispatch(fetchListSiteOrga())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableauSite);