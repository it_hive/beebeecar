import React, { Component } from 'react';
import { Icon, Grid, Menu, Table } from 'semantic-ui-react'
import Filter from "../../Filter/Filter";
import LigneSite from "./LigneSite/LigneSite"

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class TableauSite extends Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.fetchListSiteOrga(this.props.idOrganisme);
    }

    render(){
        return (
            <Table basic='very' className="table table_center">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{T.translate("app.site.nom")} </Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.site.ville")} </Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.site.adresse")} </Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.site.codePostal")} </Table.HeaderCell>
                        <Table.HeaderCell style={{width: '10em'}}> </Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {this.props.getListSite.map((item, i) =>
                        <LigneSite site={item} key={i} />
                    )}
                </Table.Body>

                <Table.Footer>
                </Table.Footer>
            </Table>
        );
    }
}

