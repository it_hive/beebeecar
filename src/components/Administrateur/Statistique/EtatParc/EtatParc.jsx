import React, { Component } from 'react';
import {Line, Pie} from 'react-chartjs-2';

export default class EtatParc extends Component{
    render(){
        const data = {
            datasets: [{
                data: [
                    this.props.data.nombreVehiculeDisponible,
                    this.props.data.nombreVehiculeIndisponible
                ],
                backgroundColor:[
                    "rgba(46, 204, 113 , 0.5)",
                    "rgba(241, 196, 15, 0.5)",
                    "rgba(231, 76, 60, 0.5)"
                ],
                borderColor: [
                    "white"
                ]
            }],
            labels: [
                'Disponible',
                'Indisponible'
            ]
        };

        let options = {
            legend: {
                position: 'bottom'
            },
            responsive: true,
            aspectRatio: 3,
            maintainAspectRatio: 1
        };

        return (
            <Pie
                height={50}
                data= {data}
                options={options}
            />
        );
    }
}