import React, { Component } from 'react';
import {Bar, Line} from 'react-chartjs-2';
import dateFns from "date-fns";
var fr = require('date-fns/locale/fr'); // Import french translation for date

export default class TauxOccupationSite extends Component{
    getRandomArbitrary() {
        return Math.random() * 255;
    }

    generateData(data){
        const taux = [];
        const color = `rgba(${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, 0.5`;
        const color2 = `rgba(${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, 0.5`;
        const color3 = `rgba(${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, 0.5`;

        data.map((item, i) => {
            taux.push({
                label: item.nom+" - En cours de validation",
                backgroundColor: color,
                borderColor: color,
                borderWidth: 1,
                hoverBackgroundColor: color,
                hoverBorderColor: color,
                stack: `Stack ${item.id}`,
                data: item.EnCoursDeValidation
            });
            taux.push({
                label: item.nom+" - Validé",
                backgroundColor: color2,
                borderColor: color2,
                borderWidth: 1,
                hoverBackgroundColor: color2,
                hoverBorderColor: color2,
                stack: `Stack ${item.id}`,
                data: item.Valide
            });
            taux.push({
                label: item.nom+" - Refusé",
                backgroundColor: color3,
                borderColor: color3,
                borderWidth: 1,
                hoverBackgroundColor: color3,
                hoverBorderColor: color3,
                stack: `Stack ${item.id}`,
                data: item.Refuse
            });
        });

        return taux;
    }

    generateLabel(){
        const labels = [];
        const day = Date.now();
        const startMonth = dateFns.subMonths(day, 11);

        for(let i = 0; i < 12; i++){
            let month = dateFns.format(dateFns.addMonths(startMonth, i), "MMMM", {locale: fr});
            labels.push(
                month.charAt(0).toUpperCase() + month.slice(1)
            );
        }

        return labels;
    }

    render(){
        var data = {
            datasets: this.generateData(this.props.data),
            labels: this.generateLabel()
        };

        let options = {
            legend: {
                position: 'bottom'
            },
            maintainAspectRatio: 1
        };

        return (
            <Bar
                height={70}
                data= {data}
                options={options}
            />
        );
    }
}