import {connect} from "react-redux";
import Statistique from './Statistique.jsx';

import {
    fetchEtatParc, fetchNbReservationEtat,
    fetchTauxOccupationSite,
    fetchTauxOccupationVehicule,
    fetchUtilisateurActif
} from "../../redux/actions/stats";
import {
    getActiveChart,
    getEtatParc, getNbReservationEtat,
    getTauxOccupationSite,
    getTauxOccupationVehicule, getUtilisateurActif
} from "../../redux/reducers/stats";

const mapStateToProps = (state) => {
    return {
        getActiveChart : getActiveChart(state),
        getTauxOccupationSite : getTauxOccupationSite(state),
        getTauxOccupationVehicule : getTauxOccupationVehicule(state),
        getEtatParc : getEtatParc(state),
        getUtilisateurActif : getUtilisateurActif(state),
        getNbReservationEtat : getNbReservationEtat(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTauxOccupationSite: () => dispatch(fetchTauxOccupationSite()),
        fetchTauxOccupationVehicule: () => dispatch(fetchTauxOccupationVehicule()),
        fetchEtatParc: () => dispatch(fetchEtatParc()),
        fetchUtilisateurActif: () => dispatch(fetchUtilisateurActif()),
        fetchNbReservationEtat: () => dispatch(fetchNbReservationEtat())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Statistique);