import React, { Component } from 'react';
import {Grid, Label, Segment} from "semantic-ui-react"
import TauxOccupationSite from "./TauxOccupationSite/TauxOccupationSite";
import TauxOccupationVehicule from "./TauxOccupationVehicule/TauxOccupationVehicule";
import EtatParc from "./EtatParc/EtatParc";
import UtilisateurActif from "./UtilisateurActif/UtilisateurActif";
import NbReservationEtat from "./NbReservationEtat/NbReservationEtat";

export default class Statistique extends Component{

    constructor(props){
        super(props);

        this.changeChart = this.changeChart.bind(this);
    }

    componentWillMount() {
        this.props.fetchTauxOccupationSite();
        this.props.fetchTauxOccupationVehicule();
        this.props.fetchEtatParc();
        this.props.fetchUtilisateurActif();
        this.props.fetchNbReservationEtat();
    }

    changeChart(chart){
        this.props.setActiveChart(chart);
    }

    render(){
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Segment className="widget">
                            <div className="header colored">
                                <p className="title">
                                    Etat du parc
                                </p>
                                <p className="description">
                                    Répartition des véhicules disponibles et indiponibles en fonction des demandes à la date du jour
                                </p>
                            </div>
                            <div className="body">
                                <EtatParc data={this.props.getEtatParc} />
                            </div>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <Segment className="widget">
                            <div className="header colored">
                                <p className="title">
                                    Nombre d'utilisateur actif
                                </p>
                                <p className="description">
                                    Nomdre d'utilisateur ayant effectué au moins une demande mois par mois
                                </p>
                            </div>
                            <div className="body">
                                <UtilisateurActif data={this.props.getUtilisateurActif} />
                            </div>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Segment className="widget">
                            <div className="header colored">
                                <p className="title">
                                    Taux d'occupation par véhicule
                                </p>
                                <p className="description">
                                    Décrit le taux d'occupation de chaque véhicule par mois (nombre de jours réservés / nombre de jours total)
                                </p>
                            </div>
                            <div className="body">
                                <TauxOccupationVehicule data={this.props.getTauxOccupationVehicule} />
                            </div>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Segment className="widget">
                            <div className="header colored">
                                <p className="title">
                                    Taux d'occupation par site
                                </p>
                                <p className="description">
                                    Décrit le taux d'ocuppation de chaque site par mois (nombre de jours réservés / nombre de jours total)
                                </p>
                            </div>
                            <div className="body">
                                <TauxOccupationSite data={this.props.getTauxOccupationSite} />
                            </div>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Segment className="widget">
                            <div className="header colored">
                                <p className="title">
                                    Nombre de réservation par site
                                </p>
                            </div>
                            <div className="body">
                                <NbReservationEtat data={this.props.getNbReservationEtat} />
                            </div>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}