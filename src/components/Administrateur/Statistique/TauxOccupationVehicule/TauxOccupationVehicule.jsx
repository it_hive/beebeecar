import React, { Component } from 'react';
import {Bar, Line} from 'react-chartjs-2';
import dateFns from "date-fns";
var fr = require('date-fns/locale/fr'); // Import french translation for date

export default class TauxOccupationVehicule extends Component{
    getRandomArbitrary() {
        return Math.random() * 255;
    }

    generateData(data){
        const taux = [];
        data.map((item, i) => {
            const color = [`rgba(${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, 0.5`];

            taux.push({
                'label': item.vehImmatriculation,
                'data' : [
                    item.tauxOccupationN11,
                    item.tauxOccupationN10,
                    item.tauxOccupationN9,
                    item.tauxOccupationN8,
                    item.tauxOccupationN7,
                    item.tauxOccupationN6,
                    item.tauxOccupationN5,
                    item.tauxOccupationN4,
                    item.tauxOccupationN3,
                    item.tauxOccupationN2,
                    item.tauxOccupationN1,
                    item.tauxOccupationN
                ],
                borderColor: color,
                backgroundColor: `rgba(0,0,0,0)`,
                pointBorderColor: color,
                pointBackgroundColor: `rgba(0,0,0,0)`,
                pointHoverBackgroundColor: `rgba(0,0,0,0)`,
                pointHoverBorderColor: color,
            })
        });

        return taux;
    }

    generateLabel(){
        const labels = [];
        const day = Date.now();
        const startMonth = dateFns.subMonths(day, 11);

        for(let i = 0; i < 12; i++){
            let month = dateFns.format(dateFns.addMonths(startMonth, i), "MMMM", {locale: fr});
            labels.push(
                month.charAt(0).toUpperCase() + month.slice(1)
            );
        }

        return labels;
    }

    render(){
        var data = {
            datasets: this.generateData(this.props.data),
            labels: this.generateLabel()
        };

        let options = {
            legend: {
                position: 'bottom'
            },
            steppedLine: true,
            aspectRation: 1,
            maintainAspectRatio: 1
        };

        return (
            <Line
                height={70}
                data= {data}
                options={options}
            />
        );
    }
}