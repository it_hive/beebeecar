import React, { Component } from 'react';
import {Bar, Line} from 'react-chartjs-2';
import dateFns from "date-fns";
var fr = require('date-fns/locale/fr'); // Import french translation for date

export default class UtilisateurActif extends Component{
    getRandomArbitrary() {
        return Math.random() * 255;
    }

    generateData(data){
        const taux = [];
        const color = `rgba(${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, ${this.getRandomArbitrary()}, 0.5`;

        data.map((item, i) => {
            taux.push({
                label: 'Nombre d\'utilisateurs actifs',
                backgroundColor: color,
                borderColor: color,
                borderWidth: 1,
                hoverBackgroundColor: color,
                hoverBorderColor: color,
                data: [
                    item.utilisateurActifsN11,
                    item.utilisateurActifsN10,
                    item.utilisateurActifsN9,
                    item.utilisateurActifsN8,
                    item.utilisateurActifsN7,
                    item.utilisateurActifsN6,
                    item.utilisateurActifsN5,
                    item.utilisateurActifsN4,
                    item.utilisateurActifsN3,
                    item.utilisateurActifsN1,
                    item.utilisateurActifsN
                ]
            });
        });

        return taux;
    }

    generateLabel(){
        const labels = [];
        const day = Date.now();
        const startMonth = dateFns.subMonths(day, 11);

        for(let i = 0; i < 12; i++){
            let month = dateFns.format(dateFns.addMonths(startMonth, i), "MMMM", {locale: fr});
            labels.push(
                month.charAt(0).toUpperCase() + month.slice(1)
            );
        }

        return labels;
    }

    render(){
        const data = {
            datasets: this.generateData(this.props.data),
            labels: this.generateLabel()
        };
        return (
            <Bar
                height={50}
                data= {data}
                options={{
                    legend: {
                        position: 'none'
                    },
                    maintainAspectRatio: 1
                }}
            />
        );
    }
}