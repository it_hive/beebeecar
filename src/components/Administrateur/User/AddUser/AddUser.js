import {connect} from "react-redux";
import AddUser from './AddUser.jsx';
import {saveUser, updateUser} from "../../../redux/actions/user";

const mapDispatchToProps = (dispatch) => {
    return {
        saveUser: (input) => dispatch(saveUser(input)),
        updateUser: (input, idUser)=> dispatch(updateUser(input, idUser))
    }
}

export default connect(undefined, mapDispatchToProps)(AddUser);