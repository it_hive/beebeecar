import React, { Component } from 'react';
import { Segment, Button, Checkbox, Form, Grid, Input, Select, Header } from 'semantic-ui-react'

let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class AddUser extends Component{

    constructor(props){
        super(props);
        this.state = {
            input: {},
            defaultName: '',
            defaultSurname: '',
            defaultMail: '',
            defaultPhone: '',
            defaultPermis: false,
            defaultAdmin: false,
            errorPassword: false
        };
        this.onClickAction = this.onClickAction.bind(this);
    }

    onClickAction(){
        if(this.props.defaultUser){
            this.props.updateUser(this.state.input, this.props.defaultUser.idUtilisateur);
            this.props.onClose()
        }
        else
            this.props.saveUser(this.state.input);
    }

    componentWillMount(){
        const defaultUser = this.props.defaultUser;
        if(defaultUser){
            const input = {
                name: defaultUser.nom,
                surname: defaultUser.prenom,
                mail: defaultUser.mail,
                phone: defaultUser.telephone,
                asPermis: defaultUser.hasPermis,
                isAdmin: defaultUser.admin};
            this.setState({
                defaultName : input.name,
                defaultSurname : input.surname,
                defaultMail : input.mail,
                defaultPhone : input.phone,
                defaultPermis : input.asPermis,
                defaultAdmin : input.isAdmin,
                input
            })
        }
    }

    handlePassword(donnee){
        const regexPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
        if(regexPassword.test(donnee)){
            this.setState({
                errorPassword: false,
                input : {...this.state.input, "password" : donnee}
            })
        }else{
            this.setState({errorPassword: true})
        }
    }

    render(){
        return (
            <Form>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4} />
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.user.name")}
                                    type="text"
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "name" : value}})}
                                    defaultValue={this.state.defaultName}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.user.surname")}
                                    type="text"
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "surname" : value}})}
                                    defaultValue={this.state.defaultSurname}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.user.mail")}
                                    type="text"
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "mail" : value}})}
                                    defaultValue={this.state.defaultMail}
                                    required
                                />
                            </Form.Field>
                            {!this.props.defaultUser && (
                                <Form.Field>
                                    <Form.Input
                                        label={T.translate("app.user.password")}
                                        type="password"
                                        onChange={ (event, {value}) => {this.handlePassword(value)}}
                                        error={this.state.errorPassword}
                                        required
                                    />
                                </Form.Field>
                            )}
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.user.phone")}
                                    onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "phone" : value}})}
                                    defaultValue={this.state.defaultPhone}
                                    required
                                    type="text"/>
                            </Form.Field>
                            <Form.Field>
                                <Checkbox
                                    label={T.translate("app.user.permis")}
                                    onChange={ (event, {checked}) =>  this.setState({input : {...this.state.input, "asPermis" : checked}})}
                                    defaultChecked={this.state.defaultPermis}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Checkbox
                                    label={T.translate("app.user.admin")}
                                    onChange={ (event, {checked}) =>  this.setState({input : {...this.state.input, "isAdmin" : checked}})}
                                    defaultChecked={this.state.defaultAdmin}
                                />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={4} />
                    </Grid.Row>
                    {this.state.errorPassword && (
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <p style={{textAlign: 'center', color: 'red'}}>Le mot de passe doit contenir au moins 8 caractères dont au moins une lettre minuscule, une lettre majuscule et un chiffre.</p>
                            </Grid.Column>
                            <Grid.Column width={6} />
                        </Grid.Row>
                    )}
                    <Grid.Row>
                        <Grid.Column width={6} />
                        <Grid.Column width={4}>
                            <Button
                                fluid
                                type='submit'
                                onClick={this.onClickAction}
                            >
                                {this.props.defaultUser?T.translate("app.user.edit"):T.translate("app.user.add")}
                            </Button>
                        </Grid.Column>
                        <Grid.Column width={6} />
                    </Grid.Row>
                </Grid>
            </Form>
        );
    }
}

