import {connect} from "react-redux";
import LigneUser from './LigneUser.jsx';
import {archiveUser} from "../../../../redux/actions/user";


const mapDispatchToProps = (dispatch) => {
    return {
        archiveUser: (idUser) => dispatch(archiveUser(idUser))
    }
}

export default connect(undefined, mapDispatchToProps)(LigneUser);