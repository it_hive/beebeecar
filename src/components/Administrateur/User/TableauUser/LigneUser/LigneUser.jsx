import React, {Component, Fragment} from 'react';
import { Grid, Table, Modal, Button } from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import dateFns, { formatRelative } from "date-fns";
import AddUser from "../../AddUser/AddUser";
let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../../translate/fr.yml'));

export default class LigneUser extends Component{

    constructor(props){
        super(props);
        this.state = {
            idUser: null,
            open: false,
            openEdit: false
        };
        this.archive = this.archive.bind(this);
        this.confirmArchive = this.confirmArchive.bind(this);
        this.close = this.close.bind(this);
        this.closeEdit = this.closeEdit.bind(this);
        this.confirmEdit = this.confirmEdit.bind(this);
    }

    archive(){
        this.props.archiveUser(this.state.idUser);
        this.close();
    }

    confirmArchive(idUser){
        this.props.open();
        this.setState({idUser, open : true});
    }

    confirmEdit(){
        this.setState({openEdit : true})
    }

    close(){
        this.setState({open: false});
    }

    closeEdit(){
        this.setState({openEdit: false})
    }

    render(){
        const user = this.props.user;
        return (
            <Fragment>
                <Table.Row>
                    <Table.Cell>{user.nom}</Table.Cell>
                    <Table.Cell>{user.prenom}</Table.Cell>
                    <Table.Cell>{user.mail}</Table.Cell>
                    <Table.Cell>{user.telephone}</Table.Cell>
                    <Table.Cell>{user.admin?T.translate("app.global.yes"):T.translate("app.global.no")}</Table.Cell>
                    <Table.Cell>
                        {!user.archived &&
                            <Fragment>
                                <Modal
                                    open={this.state.openEdit}
                                    onClose={this.closeEdit}
                                    trigger={
                                        <Button className="icon-before" onClick={this.confirmEdit} style={{float: 'left'}}>
                                            <FontAwesomeIcon icon="pencil-alt"/>
                                            Modifier
                                        </Button>
                                    }>
                                    <Modal.Header>{T.translate("app.user.editTitle")}</Modal.Header>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <AddUser defaultUser={user} onClose={this.closeEdit}/>
                                        </Modal.Description>
                                    </Modal.Content>
                                </Modal>
                                <Button className="icon-before" onClick={ () => this.confirmArchive(user.idUtilisateur)} style={{float: 'right'}}>
                                    <FontAwesomeIcon icon="times"/> {T.translate("app.user.archive")}
                                </Button>
                            </Fragment>
                        }
                    </Table.Cell>
                </Table.Row>
                <Modal
                    open={this.state.open}
                    onClose={this.close}
                >
                    <Modal.Header>
                        {T.translate("app.user.archiveTitle")}
                    </Modal.Header>
                    <Modal.Content>
                        <p>{T.translate("app.user.archiveQuestion")}</p>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button onClick={this.close} negative>
                            {T.translate("app.global.no")}
                        </Button>
                        <Button
                            onClick={this.archive}
                            positive
                            labelPosition='right'
                            icon='checkmark'
                            content={T.translate("app.global.yes")}
                        />
                    </Modal.Actions>
                </Modal>
            </Fragment>
        );
    }
}