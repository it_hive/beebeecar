import {connect} from "react-redux";
import TableauUser from './TableauUser.jsx';
import fetchUsers from '../../../redux/actions/user'

const mapStateToProps = (state, ownProps) => {
    return({
        getListUser: state.user.listUser
    });
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUsers: () => dispatch(fetchUsers())
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(TableauUser);