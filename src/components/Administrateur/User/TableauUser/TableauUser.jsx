import React, { Component } from 'react';
import { Icon, Grid, Menu, Table } from 'semantic-ui-react'
import Filter from "../../Filter/Filter";
import LigneUser from "./LigneUser/LigneUser"

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class TableauUser extends Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.fetchUsers();
    }

    render(){
        return (
            <Table basic='very' className="table table_center">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{T.translate("app.user.name")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.user.surname")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.user.mail")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.user.phone")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.user.adminTitle")}</Table.HeaderCell>
                        <Table.HeaderCell style={{width: '17em'}}> </Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {this.props.getListUser.map((item, i) =>
                        ((!item.archived || this.props.showArchive)&&<LigneUser user={item} key={i} open={this.props.open} />)
                    )}
                </Table.Body>

                <Table.Footer>
                </Table.Footer>
            </Table>
        );
    }
}

