import {connect} from "react-redux";
import User from './User.jsx';
import {getIsArchive, getIsCreate} from "../../redux/reducers/user";

const mapStateToProps = (state) => {
    return {
        isCreate : getIsCreate(state),
        isArchive: getIsArchive(state)
    }
};

export default connect(mapStateToProps, null)(User);