import React, {Component, Fragment} from 'react';
import { Segment, Button, Checkbox, Modal, Message, Grid } from 'semantic-ui-react'
import AddUser from "./AddUser/AddUser"
import TableauUser from "./TableauUser/TableauUser";

let T = require('i18n-react').default;
T.setTexts(require("json-loader!yaml-loader!./../../../translate/fr.yml"));

export default class User extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: null,
            openArchive: null,
            message: true,
            showArchive: false
        };
        this.handleShow = this.handleShow.bind(this);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.openArchive = this.openArchive.bind(this);
    }

    handleShow(e){
        this.setState({showArchive: !this.state.showArchive});
    }

    componentDidUpdate(){
        if((this.props.isCreate === 200 || this.props.isArchive === 200) && (this.state.open || this.state.openArchive)){
            this.setState({ open: false, message: false, openArchive: false });
            setTimeout(() => {
                this.setState({ message: true })
            }, 3000);
        }
    }

    open(){
        this.setState({ open: true })
    }
    close(){
        this.setState({ open: false })
    }

    openArchive(){
        this.setState({openArchive: true})
    }

    render(){
        return (
            <Fragment>
                <Message
                    positive
                    hidden = {this.state.message}
                    onDismiss={this.handleDismiss}
                    header={this.props.isCreate === 200?T.translate("app.user.isCreate"):T.translate("app.user.isArchive")}
                    content={this.props.isCreate === 200?T.translate("app.user.isCreateLong"):T.translate("app.user.isArchiveLong")}
                />
                <Segment>
                    <div className="header colored">
                        <div className="flex flex-row">
                            <p className="title flex-start">
                                {T.translate("app.user.listMessage")}
                            </p>
                            <div className="flex-end">
                                <Modal open={this.state.open}
                                       onOpen={this.open}
                                       onClose={this.close}
                                       trigger={<Button className="addVeh">{T.translate("app.user.addTitle")}</Button>}>
                                    <Modal.Header>{T.translate("app.user.addTitle")}</Modal.Header>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <AddUser/>
                                        </Modal.Description>
                                    </Modal.Content>
                                </Modal>
                            </div>
                        </div>
                    </div>
                    <div className="body">
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <Checkbox
                                        label={T.translate("app.user.showHidden")}
                                        onChange={this.handleShow}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <TableauUser showArchive={this.state.showArchive} open={this.openArchive}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                </Segment>
            </Fragment>
        );
    }
}

