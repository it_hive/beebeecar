import {connect} from "react-redux";
import AjouterVehicule from './AjouterVehicule.jsx';

import {fetchMarques} from './../../../redux/actions/vehicule'
import {getMarques, getModeles} from "../../../redux/reducers/vehicule";
import {fetchCreateVeh, fetchModele, fetchUpdateVeh} from "../../../redux/actions/vehicule";
import {fetchListSiteOrga} from "../../../redux/actions/site";
import {getListSiteOrga} from "../../../redux/reducers/site";
import {getUserInfos} from "../../../redux/reducers/connexion";

const mapStateToProps = (state, props) => {
    return {
        getMarques: getMarques(state),
        getModeles: getModeles(state),
        getListSiteOrga: getListSiteOrga(state),
        idOrganisme: getUserInfos(state).idOrganisme
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMarques: () => dispatch(fetchMarques()),
        fetchModele: (idMarque) => dispatch(fetchModele(idMarque)),
        fetchListSiteOrga: () => dispatch(fetchListSiteOrga()),
        fetchCreateVeh: (input) => dispatch(fetchCreateVeh(input)),
        fetchUpdateVeh: (input, idVeh) => dispatch(fetchUpdateVeh(input, idVeh))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AjouterVehicule);