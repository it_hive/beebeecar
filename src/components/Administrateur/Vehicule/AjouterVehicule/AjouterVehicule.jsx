import React, { Component } from 'react';
import { Form, Grid, Button, TextArea } from 'semantic-ui-react'
import HivePicker from "../../../hiveComponents/HivePicker/HivePicker";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class Vehicule extends Component{

    constructor(props){
        super(props);
        this.state = {
            siv: null,
            input: {},
            errorSIV: false,
            errorMIleage: false
        };

        this.optionSite = this.optionSite.bind(this);
        this.optionModel = this.optionModel.bind(this);
        this.optionMarque = this.optionMarque.bind(this);
        this.saveInput = this.saveInput.bind(this);
        this.handleSIV = this.handleSIV.bind(this);
        this.controlSIV = this.controlSIV.bind(this);
        this.mileageBlur = this.mileageBlur.bind(this);
        this.onClickACtion = this.onClickACtion.bind(this);
        this.handleMark = this.handleMark.bind(this);
        this.picker = this.picker.bind(this)
    }

    componentWillMount(){
        this.props.fetchMarques();
        this.props.fetchListSiteOrga(this.props.idOrganisme);
        if(this.props.vehicule){
            this.handleMark(this.props.vehicule.idMarque);
            this.setState({input: this.props.vehicule})
        }
    }

    picker(color){
        this.saveInput('color', color);
    }

    saveInput(name, data){
        this.setState({input : {...this.state.input, [name] : data}})
    }


    handleMark(value){
        this.props.fetchModele(value);
        this.saveInput('marque', value);
    }

    controlSIV(e){
        const regexSIV = RegExp('[A-Z]{2}-[0-9]{3}-[A-Z]{2}');
        if(!regexSIV.test(e.target.value))
            this.setState({errorSIV: true});
        else
            this.setState({errorSIV: false});
    }

    handleSIV(e){
        let siv = e.target.value;
        let longueur = siv.length;
        if((longueur === 2 || longueur === 6) && this.state.siv.length < longueur)
            siv += '-';
        else if(longueur === 10)
            siv = siv.slice(0, -1);
        this.saveInput('siv', siv.toUpperCase());
        this.setState({siv: siv.toUpperCase()})
    }

    mileageBlur(e){
        let mileage = e.target.value;
        if(mileage.length > 6)
            this.setState({errorMIleage: true});
        else
            this.setState({errorMIleage: false});
    }

    onClickACtion(){
        if(this.props.vehicule === undefined)
            this.props.fetchCreateVeh(this.state.input);
        else{
            this.props.fetchUpdateVeh(this.state.input, this.state.input.id_vehicule);
            this.props.onClose();
        }
    }

    optionSite(){
        let option = [];
        const listSite = this.props.getListSiteOrga;
        listSite && listSite.forEach(item => {
            option.push({
                key: item.sitId,
                text: item.nom,
                value: item.sitId
            })
        });
        return option;
    }

    optionModel(){
        let option = [];
        const listModele = this.props.getModeles;
        listModele && listModele.forEach(item => {
            option.push({
                key: item.modId,
                text: item.libelle,
                value: item.modId
            })
        });
        return option;
    }

    optionMarque(){
        let option = [];
        const listMarques = this.props.getMarques;
        listMarques && listMarques.forEach(item => {

            option.push({
                key: item.marId,
                text: item.marLibelle,
                value: item.marId
            })
        });
        return option;
    }

    formatDate(date){
        if(date){
            date = date.split("/");
            //const d = new Date(date[0], date[1], date[2].substr(0, 2));
            return( new Date(( date[0] + '-' + date[1] + '-' + date[2].substr(0, 2))) );
        }
    }

    render(){
        let vehicule = this.props.vehicule;
        let edit = true;
        if(vehicule === undefined){
            vehicule = {};
            edit = false;
        }

        const optionMark = this.optionMarque();
        const optionModel = this.optionModel();
        const optionSite = this.optionSite();

        return (
            <Form>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4} />
                        <Grid.Column width={8}>
                            <Form.Field>
                                <Form.Input type="text"
                                    label={T.translate("app.car.id")}
                                    onBlur={this.controlSIV}
                                    onChange={this.handleSIV}
                                    value={this.state.siv}
                                    error={this.state.errorSIV}
                                    defaultValue={vehicule.immatriculation}
                                    disabled={edit}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Select className="select"
                                     label={T.translate("app.car.mark")}
                                     options={optionMark}
                                     onChange={ (event, {value}) => (this.handleMark(value))}
                                     defaultValue={vehicule.idMarque}
                                     disabled={edit}
                                     required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Select className="select"
                                    label={T.translate("app.car.model")}
                                    onChange={ (event, {value}) => (this.saveInput('idModele', value))}
                                    options={optionModel}
                                    defaultValue={vehicule.idModele}
                                    disabled={edit}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.car.mileage")}
                                    onBlur={this.mileageBlur}
                                    error={this.state.errorMIleage}
                                    onChange={ (event, {value}) => (this.saveInput('kilometrage', value))}
                                    defaultValue={vehicule.kilometrage}
                                    type="text"
                                    max={6}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    label={T.translate("app.car.date")}
                                    onChange={ (event, {value}) => (this.saveInput('dateMiseEnService', value))}
                                    defaultValue={this.formatDate(vehicule.dateMiseEnService)}
                                    disabled={edit}
                                    type="date"/>
                            </Form.Field>
                            <Form.Field>
                                <Form.Select className="select"
                                     label={T.translate("app.car.site")}
                                     options={optionSite}
                                     onChange={ (event, {value}) => (this.saveInput('idSite', value))}
                                     defaultValue={vehicule.idSite}
                                     required
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{T.translate("app.car.color")}</label>
                                <HivePicker
                                    color={(vehicule&&vehicule.couleur)}
                                    handleChange={this.picker}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{T.translate("app.car.details")}</label>
                                <TextArea
                                    onChange={ (event, {value}) => (this.saveInput('details', value))}
                                    defaultValue={vehicule.details}
                                />
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column width={4} />
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={6} />
                        <Grid.Column width={4}>
                            <Button
                                fluid
                                type='submit'
                                onClick={this.onClickACtion}
                            >
                                {(vehicule.immatriculation === undefined?T.translate("app.car.add"):T.translate("app.car.edit"))}
                            </Button>
                        </Grid.Column>
                        <Grid.Column width={6} />
                    </Grid.Row>
                </Grid>
            </Form>
        );
    }
}
