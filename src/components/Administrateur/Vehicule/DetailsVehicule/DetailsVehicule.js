import {connect} from "react-redux";
import DetailsVehicule from './DetailsVehicule.jsx';
import {deleteVeh} from "../../../redux/actions/vehicule";

const mapDispatchToProps = (dispatch) => {
    return {
        deleteVeh: (idVeh) => dispatch(deleteVeh(idVeh))
    }
}

export default connect(undefined, mapDispatchToProps)(DetailsVehicule);