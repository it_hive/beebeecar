import React, { Component, Fragment } from 'react';
import {Modal, Grid, Button, TextArea, Segment, Table, Header, Image} from 'semantic-ui-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Icon from '../../../../../public/images/car.jpg';
import AjouterVehicule from "./../AjouterVehicule/AjouterVehicule"
import dateFns from "date-fns";
import {Link, Redirect} from "react-router-dom";
let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class DetailsVehicule extends Component{
    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            idVeh: null,
            open: false
        };
        this.supprimer = this.supprimer.bind(this);
        this.close = this.close.bind(this);
    }

    componentWillMount() {
        if(this.props.location.state === undefined){
            this.setState({
                redirect: true
            });
        }
    }

    confirmDelete(idVeh){
        this.setState({idVeh, open : true});
    }

    supprimer(){
        this.props.deleteVeh(this.state.idVeh);
        this.setState({redirect : true});
    }

    close(){
        this.setState({open: false});
    }

    redirectToCar(){
        if(this.state.redirect){
            this.setState({redirect: false});
            return <Redirect to="/car"/>
        }
    }

    render(){
        let vehicule = null;
        if(!this.state.redirect){
            vehicule = this.props.location.state.v;
        }

        return(
            <Fragment>
                {!this.state.redirect ? (
                    <Segment className="fullPage vehicule_details">
                        <div className="header flex flex-row">
                            <div className="flex-start">
                                <Link to={"/car"}>
                                    <Button className="margin-top-none icon-before"><FontAwesomeIcon icon="arrow-left"/> Retour</Button>
                                </Link>
                            </div>
                            <div className="flex-end">
                                <Grid columns='equal'  textAlign='center' >
                                    <Grid.Column className="pointer">
                                        <Modal trigger={
                                            <Button className="margin-top-none icon-before">
                                                <FontAwesomeIcon icon="pencil-alt"/>
                                                Modifier
                                            </Button>
                                        }>
                                            <Modal.Header>{T.translate("app.car.editTitle")}</Modal.Header>
                                            <Modal.Content>
                                                <Modal.Description>
                                                    <AjouterVehicule vehicule={vehicule}/>
                                                </Modal.Description>
                                            </Modal.Content>
                                        </Modal>
                                    </Grid.Column>
                                    <Grid.Column className="pointer" onClick={ () => this.confirmDelete(vehicule.id_vehicule)}>
                                        <Button className="margin-top-none icon-before">
                                            <FontAwesomeIcon icon="times"/>
                                            Supprimer
                                        </Button>
                                        <Modal
                                            open={this.state.open}
                                            onClose={this.close}
                                        >
                                            <Modal.Header>{T.translate("app.car.deleteTitle")}</Modal.Header>
                                            <Modal.Content>
                                                <p>{T.translate("app.car.deleteQuestion")}</p>
                                            </Modal.Content>
                                            <Modal.Actions>
                                                <Button onClick={this.close} negative>
                                                    {T.translate("app.global.no")}
                                                </Button>
                                                <Button
                                                    onClick={this.supprimer}
                                                    positive
                                                    labelPosition='right'
                                                    icon='checkmark'
                                                    content={T.translate("app.global.yes")}
                                                />
                                            </Modal.Actions>
                                        </Modal>
                                    </Grid.Column>
                                </Grid>
                            </div>
                        </div>
                        <div className="body">
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={16}>
                                        <div className="flex flex-column">
                                            <div className="flex-center">
                                                <img src={ Icon } alt="" className="image"/>
                                            </div>
                                            <div className="flex-center">
                                                <p className="title">
                                                    {vehicule.marque} - {vehicule.modele}
                                                </p>
                                            </div>
                                        </div>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </div>
                    </Segment>
                ) : (
                    this.redirectToCar()
                )}
            </Fragment>
        )
    }
}