import {connect} from "react-redux";
import LigneVehicule from './LigneVehicule.jsx';
import {deleteVeh} from "../../../../redux/actions/vehicule";

const mapDispatchToProps = (dispatch) => {
    return {
        deleteVeh: (idVeh, dateFin) => dispatch(deleteVeh(idVeh, dateFin))
    }
}

export default connect(undefined, mapDispatchToProps)(LigneVehicule);