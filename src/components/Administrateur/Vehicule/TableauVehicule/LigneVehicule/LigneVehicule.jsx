import React, {Component, Fragment} from 'react';
import { Form, Table, Modal, Button, Grid } from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import dateFns, { formatRelative } from "date-fns";
import AjouterVehicule from "./../../AjouterVehicule/AjouterVehicule"

let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../../translate/fr.yml'));

export default class LigneVehicule extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: false,
            openArchive: false,
            dateFin: null
        };
        this.closeEdit = this.closeEdit.bind(this);
        this.confirmEdit = this.confirmEdit.bind(this);
        this.confirmArchive = this.confirmArchive.bind(this);
        this.closeArchive = this.closeArchive.bind(this);
        this.archive = this.archive.bind(this);
        this.saveDateFin = this.saveDateFin.bind(this);
    }

    confirmEdit(){
        this.setState({open: true});
    }

    closeEdit(){
        this.setState({open: false});
    }

    confirmArchive(){
        this.props.open();
        this.setState({openArchive: true});
    }

    closeArchive(){
        this.setState({openArchive: false});
    }

    archive(){
        this.props.deleteVeh(this.props.vehicule.id_vehicule, this.state.dateFin);
        this.closeArchive();
    }

    saveDateFin(dateFin){
        this.setState({dateFin});
    }

    render(){
        const vehicule = this.props.vehicule;
        return (
            <Fragment>
                <Table.Row>
                    <Table.Cell>{vehicule.immatriculation}</Table.Cell>
                    <Table.Cell>{vehicule.marque} {vehicule.modele}</Table.Cell>
                    <Table.Cell>{vehicule.kilometrage}</Table.Cell>
                    <Table.Cell>{vehicule.nomSite}</Table.Cell>
                    <Table.Cell>{dateFns.format(vehicule.dateMiseEnService, 'DD/MM/YYYY')}</Table.Cell>
                    <Table.Cell>
                        {
                            (this.props.vehiculeIsDispo(vehicule.dateFinService) &&
                                <Modal
                                    open={this.state.open}
                                    onClose={this.closeEdit}
                                    trigger={
                                        <Button className="margin-top-none icon-before" onClick={this.confirmEdit} style={{float: 'left'}}>
                                            <FontAwesomeIcon icon="pencil-alt"/>
                                            Modifier
                                        </Button>
                                    }>
                                    <Modal.Header>{T.translate("app.car.editTitle")}</Modal.Header>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <AjouterVehicule vehicule={vehicule} onClose={this.closeEdit}/>
                                        </Modal.Description>
                                    </Modal.Content>
                                </Modal>
                            )
                        }
                        {
                            (!vehicule.dateFinService &&
                                <Button className="margin-top-none icon-before" onClick={this.confirmArchive} style={{float: 'right'}}>
                                    <FontAwesomeIcon icon="times"/>
                                    {T.translate("app.car.archive")}
                                </Button>
                            )
                        }
                    </Table.Cell>
                </Table.Row>
                <Modal
                    open={this.state.openArchive}
                    onClose={this.closeArchive}
                >
                    <Modal.Header>{T.translate("app.car.deleteQuestion")}</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={4} />
                                    <Grid.Column width={8}>
                                        <Form.Input
                                            label={T.translate("app.car.dateEnd")}
                                            onChange={ (event, {value}) => (this.saveDateFin(value))}
                                            required
                                            type="date"/>
                                    </Grid.Column>
                                    <Grid.Column width={4} />
                                </Grid.Row>
                                <Grid.Row>
                                    <Grid.Column width={10} />
                                    <Grid.Column width={6}>
                                        <Button onClick={this.close} negative>
                                            {T.translate("app.global.no")}
                                        </Button>
                                        <Button
                                            onClick={this.archive}
                                            positive
                                            labelPosition='right'
                                            icon='checkmark'
                                            content={T.translate("app.global.yes")}
                                        />
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Form>
                    </Modal.Content>
                </Modal>
            </Fragment>
        );
    }
}