import {connect} from "react-redux";
import TableauVehicule from './TableauVehicule.jsx';
import fetchVehicules from "../../../redux/actions/vehicule";
import {getListVeh} from "../../../redux/reducers/vehicule";

const mapStateToProps = (state, props) => {
    return {
        getListVeh: getListVeh(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchVehicules: () => dispatch(fetchVehicules())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableauVehicule);