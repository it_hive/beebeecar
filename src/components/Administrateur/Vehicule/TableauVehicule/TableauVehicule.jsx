import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Icon, Grid, Menu, Table } from 'semantic-ui-react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Filter from "../../Filter/Filter";
import LigneVehicule from "./LigneVehicule/LigneVehicule"

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class TableauVehicule extends Component{

    constructor(props){
        super(props);
        this.vehiculeIsDispo = this.vehiculeIsDispo.bind(this);
    }

    componentWillMount(){
        this.props.fetchVehicules();
    }

    vehiculeIsDispo(dateFinService){
        if(dateFinService){
            let jourFinService = dateFinService.substr(0, 2);
            let moisFinService = dateFinService.substr(3, 2);
            let anneeFinService = dateFinService.substr(6, 4);
            let dateFin = new Date(anneeFinService, moisFinService, jourFinService);
            let dateJour = new Date();

            return(dateFin > dateJour);
        }else
            return true;

    }

    render(){
        return (
            <Table basic='very' className="table table_center">
            {/*<Table celled striped>*/}
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{T.translate("app.car.id")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.car.mark")} / {T.translate("app.car.model")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.car.mileage")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.car.site")}</Table.HeaderCell>
                        <Table.HeaderCell>{T.translate("app.car.date")}</Table.HeaderCell>
                        <Table.HeaderCell style={{width: '17em'}}> </Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {this.props.getListVeh.map((item, i) =>
                        ((this.vehiculeIsDispo(item.dateFinService) || this.props.showArchive) && <LigneVehicule vehicule={item} key={i} vehiculeIsDispo={this.vehiculeIsDispo} open={this.props.open}/>)
                    )}
                </Table.Body>

                <Table.Footer>
                </Table.Footer>
            </Table>
        );
    }
}


TableauVehicule.propTypes = {
    fetchVehicules : PropTypes.func,
    getListVeh : PropTypes.array
}

