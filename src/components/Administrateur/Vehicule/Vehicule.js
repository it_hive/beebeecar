import {connect} from "react-redux";
import Vehicule from './Vehicule.jsx';
import {getIsCreate, getIsSuppr} from "../../redux/reducers/vehicule";

const mapStateToProps = (state) => {
    return {
        isCreate : getIsCreate(state),
        isSuppr : getIsSuppr(state)
    }
};

export default connect(mapStateToProps, null)(Vehicule);