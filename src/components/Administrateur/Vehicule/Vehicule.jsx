import React, {Component, Fragment} from 'react';
import { Segment, Button, Modal, Message, Grid, Checkbox } from 'semantic-ui-react'
import TableauVehicule from "./TableauVehicule/TableauVehicule";
import AjouterVehicule from "./AjouterVehicule/AjouterVehicule"

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require("json-loader!yaml-loader!../../../translate/fr.yml"));

export default class Vehicule extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: null,
            message: true,
            showArchive: false,
            openArchive: null
        };
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.openArchive = this.openArchive.bind(this);
    }

    componentDidUpdate(){
        if((this.props.isCreate === 200 || this.props.isSuppr === 200) && (this.state.open || this.state.openArchive)){
            this.setState({ open: false, message: false, openArchive: false });
            setTimeout(() => {
                this.setState({ message: true })
            }, 3000);
        }
    }

    open(){
        this.setState({ open: true })
    }
    close(){
        this.setState({ open: false })
    }

    handleShow(){
        this.setState({showArchive: !this.state.showArchive});
    }

    openArchive(){
        this.setState({openArchive: true})
    }

    render(){
        return (
            <Fragment>
                <Message
                    positive
                    hidden = {this.state.message}
                    onDismiss={this.handleDismiss}
                    header={this.props.isCreate == 200?T.translate("app.car.isCreate"):T.translate("app.car.isDelete")}
                    content={this.props.isCreate == 200?T.translate("app.car.isCreateLong"):T.translate("app.car.isDeleteLong")}
                />
                <Segment className="fullPage">
                    <div className="header colored">
                        <div className="flex flex-row">
                            <p className="title flex-start">
                                Liste des véhicules
                            </p>
                            <div className="flex-end">
                                <Modal  open={this.state.open}
                                        onOpen={this.open}
                                        onClose={this.close}
                                        trigger={<Button className="addVeh">{T.translate("app.car.addTitle")}</Button>}>
                                    <Modal.Header>{T.translate("app.car.addTitle")}</Modal.Header>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <AjouterVehicule />
                                        </Modal.Description>
                                    </Modal.Content>
                                </Modal>
                            </div>
                        </div>
                    </div>
                    <div className="body">
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <Checkbox
                                        label={T.translate("app.car.showHidden")}
                                        onChange={this.handleShow}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column>
                                    <TableauVehicule showArchive={this.state.showArchive} open={this.openArchive}/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                </Segment>
            </Fragment>
        );
    }
}

