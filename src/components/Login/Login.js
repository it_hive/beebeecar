import {connect} from "react-redux";
import Login from './Login.jsx';

import {seConnecter, setAuthError} from "../redux/actions/connexion";
import {getAuthError, getUserInfos} from "../redux/reducers/connexion";

const mapStateToProps = (state, ownProps) => {
    return({
        state: state,
        cookies: ownProps.cookies,
        codeErreur: (getUserInfos(state)?getUserInfos(state).code:null),
        msgError: getAuthError(state)
    });
};

const mapDispatchToProps = (dispatch) => {
    return {
        seConnecter: (login, password) => dispatch(seConnecter(login, password)),
        setAuthError: (message) => dispatch(setAuthError(message))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);