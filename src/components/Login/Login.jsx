import React, { Component } from 'react';
import { Message, Input, Button, Checkbox, Form } from 'semantic-ui-react';
import Logo from "../../../public/images/LogoNoir.png";
import Background from "../../../public/images/Background_beebeecar2.svg";

let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../translate/fr.yml'));

export default class Login extends Component{

    constructor(props){
        super(props);
        this.state = {
            login: null,
            password: null,
            error: "",
            errorMail: false
        };
        this.onClickAction = this.onClickAction.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
    }

    onClickAction(){
        if(!this.state.errorMail){
            const t = this.props.seConnecter(this.state.login, this.state.password);
            if(t !== ""){
                this.setState({error: t});
            }
        }else{
            this.props.setAuthError("Veuillez saisir une adresse email valide.");
        }
    }

    handleLogin(e){
        const regexMail = RegExp('^\\w+(.\\w+)*@(\\w+.)+\\w{2,4}$');
        if(!regexMail.test(e.target.value)){
            this.setState({errorMail: true});
        }else{
            this.setState({login: e.target.value, errorMail: false});
            this.props.setAuthError("");
        }
    }

    handlePassword(event){
        this.setState({password: event.target.value});
    }

    render(){
        return (
            <div id="login" style={{backgroundImage: `url(${Background})`}}>
                <div className="background"/>
                <div className="login_form">
                    <div className="box">
                        <div className="login_logo">
                            <img src={Logo} alt="BeeBeeCar"/>
                        </div>
                        <Message
                            negative
                            hidden={!this.props.codeErreur}
                            content={T.translate("erreur." + this.props.codeErreur)}
                        />
                        <Form>
                            <Form.Field>
                                <Form.Input
                                    type="text"
                                    label={T.translate("login.identifiant")}
                                    placeholder='exemple@mail.fr'
                                    error={this.state.errorMail}
                                    onChange={this.handleLogin}
                                    required
                                />
                            </Form.Field>
                            <Form.Field>
                                <Form.Input
                                    type="password"
                                    label={T.translate("login.password")}
                                    placeholder='Mot de passe'
                                    onChange={this.handlePassword}
                                    required
                                />
                            </Form.Field>
                            {/*<div className="flex flex-row">
                                <div className="flex-start">
                                    <Checkbox label={T.translate("login.stayConnected")} />
                                </div>
                                <div className="flex-end forgot_password">
                                    <a href="#">{T.translate("login.forgotPassword")}</a>
                                </div>
                            </div>*/}
                            <div className="center">
                                <div>
                                    <p id="error" className={this.props.msgError !== "" ? '' : 'hidden'}>{this.props.msgError}</p>
                                </div>
                                <div>
                                    <Button type='submit' onClick={this.onClickAction}>{T.translate("login.submit")}</Button>
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}