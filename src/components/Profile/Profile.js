import {connect} from "react-redux";
import Profile from './Profile.jsx';
import {getUserInfos} from "../redux/reducers/connexion";
import {updateUser, updatePassword} from "../redux/actions/user";

const mapStateToProps = (state) => {
    return {
        userInfos : getUserInfos(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateUser: (input, idUser) => dispatch(updateUser(input, idUser)),
        updatePassword: (newPassword, mail, idUser) => dispatch(updatePassword(newPassword, mail, idUser))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);