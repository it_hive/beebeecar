import React, { Component, Fragment } from 'react';
import {Segment, Grid, Form, Button, Message} from 'semantic-ui-react';

let T = require('i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../translate/fr.yml'));

export default class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            input: {},
            newPassword: null,
            newPasswordRepeat: null,
            errorMessage: true,
            doneMessage: true,
            doneMessageGlobal: true
        };
        this.updateInfos = this.updateInfos.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
    }

    updateInfos(){
        this.props.updateUser(this.state.input, this.props.userInfos.idUtilisateur);
        this.setState({doneMessageGlobal: false});
        setTimeout(() => {
            this.setState({ doneMessageGlobal: true })
        }, 3000);
    }

    updatePassword(){
        if(this.state.newPassword === this.state.newPasswordRepeat){
            this.props.updatePassword(this.state.newPassword, this.props.userInfos.mail, this.props.userInfos.idUtilisateur);
            this.setState({errorMessage: true, doneMessage: false});
            setTimeout(() => {
                this.setState({ doneMessage: true })
            }, 3000);
        }
        else
            this.setState({errorMessage: false, doneMessage: true});
            setTimeout(() => {
                this.setState({ errorMessage: true })
            }, 3000);
    }

    componentWillMount(){
        const userInfos = this.props.userInfos;
        const input = {
            isAdmin: userInfos.admin,
            hasPermis: userInfos.hasPermis,
            idUtilisateur: userInfos.idUtilisateur,
            name : userInfos.nom,
            surname : userInfos.prenom,
            mail : userInfos.mail,
            phone : userInfos.telephone
        };

        this.setState({input});
    };

    render(){
        const user = this.props.userInfos;

        return (
            <Fragment>
                <Grid>
                    <Grid.Row>
                        <Grid.Column computer={8} tablet={16} mobile={16}>
                            <Segment className="widget widget_center">
                                <div className="header colored">
                                    <p className="title">
                                        Mes informations
                                    </p>
                                </div>
                                <div className="body">
                                    <Form>
                                        <Message
                                            positive
                                            hidden = {this.state.doneMessageGlobal}
                                            header={"Modification effectuée"}
                                            content={"Vos information ont été modifiés avec succès"}
                                        />
                                        <Form.Field>
                                            <Form.Input type="text"
                                                        label={T.translate("app.user.name")}
                                                        defaultValue={user.nom}
                                                        onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "name" : value}})}
                                                        required
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <Form.Input type="text"
                                                        label={T.translate("app.user.surname")}
                                                        defaultValue={user.prenom}
                                                        onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "surname" : value}})}
                                                        required
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <Form.Input type="text"
                                                        label={T.translate("app.user.mail")}
                                                        defaultValue={user.mail}
                                                        onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "mail" : value}})}                                                        required
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <Form.Input type="text"
                                                        label={T.translate("app.user.phone")}
                                                        defaultValue={user.telephone}
                                                        onChange={ (event, {value}) =>  this.setState({input : {...this.state.input, "phone" : value}})}
                                                        required
                                            />
                                        </Form.Field>
                                        <Button fluid type='submit' onClick={this.updateInfos}>{T.translate("app.user.editInfos")}</Button>
                                    </Form>
                                </div>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column computer={8} tablet={16} mobile={16}>
                            <Segment className="widget widget_center">
                                <div className="header colored">
                                    <p className="title">
                                        Mon mot de passe
                                    </p>
                                </div>
                                <div className="body">
                                    <Form>
                                        <Message
                                            positive
                                            hidden = {this.state.doneMessage}
                                            header={"Modification effectuée"}
                                            content={"Votre mot de passe a été modifié avec succès"}
                                        />
                                        <Form.Field>
                                            <Form.Input type="password"
                                                        label={T.translate("app.user.password")}
                                                        onChange={ (event, {value}) =>  this.setState({newPassword :  value})}
                                                        required
                                            />
                                        </Form.Field>
                                        <Form.Field>
                                            <Form.Input type="password"
                                                        onChange={ (event, {value}) =>  this.setState({newPasswordRepeat :  value})}
                                                        required
                                            />
                                        </Form.Field>
                                        <Message
                                            negative
                                            hidden = {this.state.errorMessage}
                                            header={"Modification impossible"}
                                            content={"Les mots de passe doivent être identique"}
                                        />
                                        <Button fluid type='submit' onClick={this.updatePassword}>{T.translate("app.user.editPassword")}</Button>
                                    </Form>
                                </div>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Fragment>
        )
    }
}