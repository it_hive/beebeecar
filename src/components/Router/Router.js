import {connect} from "react-redux";
import Router from './Router.jsx';
import { isAuthenticate, isAdmin } from '../redux/reducers/connexion';
import { getOpenMenuDashboard, getMenuChoice } from '../redux/reducers/menu';

const mapStateToProps = (state) => {
   return {
        isAuthenticate : isAuthenticate(state),
        isAdmin : isAdmin(state),
        isMenuOpen: getOpenMenuDashboard(state),
        getMenuChoice: getMenuChoice(state)
    }
};

export default connect(mapStateToProps)(Router);