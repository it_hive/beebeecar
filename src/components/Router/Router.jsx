import React, { Component, Fragment } from 'react';
import { BrowserRouter } from 'react-router-dom';
import {Grid, Container, Message} from 'semantic-ui-react'
import { Switch, Route, Redirect  } from 'react-router-dom'

// Admin
import AdminMenu from "../Administrateur/Menu/Menu";
import AdminHeader from "../Administrateur/Header/Header";
import Login from "../Login/Login";
import Dashboard from "../Administrateur/Dashboard/Dashboard";
import Map from "../Administrateur/Carte/Carte";
import User from "../Administrateur/User/User";
import Vehicule from "../Administrateur/Vehicule/Vehicule";
import DetailsVehicule from "../Administrateur/Vehicule/DetailsVehicule/DetailsVehicule";
import Calendrier from "../Administrateur/Calendrier/Calendrier";
import Demande from "../Administrateur/Demande/Demande";
import Site from "../Administrateur/Site/Site";
import Profile from "../Profile/Profile";
import Statistique from "../Administrateur/Statistique/Statistique";

// User
import UserMenu from "../User/Menu/UserMenu";
import Homepage from "../User/Homepage/Homepage";
import FloatingButton from "../User/FloatingButton/FloatingButton";
import UserDemande from "../User/Demande/Demande";
import UserReservation from "../User/Reservation/Reservation";
import AddDemande from "../User/AddDemande/AddDemande";
import Covoit from "../User/Covoit/Covoit";

export default class Router extends Component{

    constructor(props){
        super(props);
    }

    render(){
        const MenuClass = this.props.isMenuOpen ? 'dashboard open' : 'dashboard';
        return (
            <BrowserRouter>
                {this.props.isAuthenticate ?
                    (this.props.isAdmin ?
                        (
                            <div className={MenuClass}>
                                <AdminMenu/>
                                <div className="body">
                                    <AdminHeader/>
                                    <div id="zoneBody">
                                        <Switch>
                                            <Route exact path='/' component={Dashboard}/>
                                            <Route exact path='/demande' component={Demande}/>
                                            <Route exact path='/stats' component={Statistique} />
                                            {/*<Route exact path='/map' component={Map} />*/}
                                            <Route exact path='/user' component={User} />
                                            <Route exact path='/car' component={Vehicule} />
                                            <Route exact path='/car/details' component={DetailsVehicule} />
                                            <Route exact path='/calendar' component={Calendrier} />
                                            <Route exact path='/profile' component={Profile} />
                                            <Route exact path='/sites' component={Site}/>
                                            <Redirect to='/'/>
                                        </Switch>
                                    </div>
                                </div>
                            </div>
                        ) :
                        (
                            <div className="user">
                                <UserMenu />
                                <section>
                                    <Container>
                                        <Switch>
                                            <Route exact path='/' component={Homepage}/>
                                            <Route exact path='/demande' component={UserDemande} />
                                            <Route exact path='/new' component={AddDemande} />
                                            <Route exact path='/covoiturage' component={Covoit} />
                                            <Route exact path='/reservation' component={UserReservation} />
                                            <Route exact path='/profil' component={Profile} />
                                            <Redirect to='/'/>
                                        </Switch>
                                    </Container>
                                </section>
                                {this.props.getMenuChoice !== "new" && (
                                    <FloatingButton/>
                                )}
                            </div>
                        )
                    ) :
                    (
                        <Grid>
                            <Grid.Column width={16}>
                                <Switch>
                                    <Route exact path='/' component={Login}/>
                                    <Redirect to='/'/>
                                </Switch>
                            </Grid.Column>
                        </Grid>
                    )
                }
            </BrowserRouter>
        );
    }
}