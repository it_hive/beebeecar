import {connect} from "react-redux";
import AddDemande from './AddDemande.jsx';
import {getUserInfos, getUtilisateurToken} from "../../redux/reducers/connexion";
import {getListSiteOrga} from "../../redux/reducers/site";
import {fetchListSiteOrga} from "../../redux/actions/site";
import {addDemandeUtilisateur, fetchDemandeVehiculeDispo} from "../../redux/actions/demandeUtilisateur";
import {getDemandeIsCreate, getDemandeVehiculeDispo} from "../../redux/reducers/demandeUtilisateur";

const mapStateToProps = (state, ownProps) => {
    return({
        getUtilisateurToken: getUtilisateurToken(state),
        idOrganisme: getUserInfos(state).idOrganisme,
        getListSite: getListSiteOrga(state),
        getDemandeVehiculeDispo: getDemandeVehiculeDispo(state),
        getDemandeIsCreate : getDemandeIsCreate(state)
    });
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchListSiteOrga: () => dispatch(fetchListSiteOrga()),
        fetchDemandeVehiculeDispo: (idOrga, dateDebut, dateFin) => dispatch(fetchDemandeVehiculeDispo(idOrga, dateDebut, dateFin)),
        addDemandeUtilisateur: (token, input) => dispatch(addDemandeUtilisateur(token, input))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddDemande);