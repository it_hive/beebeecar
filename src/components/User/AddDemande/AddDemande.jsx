import React, { Component, Fragment } from 'react';
import {Button, Form, Grid, Icon, Menu, Modal, Segment, Table} from "semantic-ui-react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import dateFns, { formatRelative } from "date-fns";
import {Redirect} from "react-router-dom";

export default class AddDemande extends Component{
    constructor(props){
        super(props);
        this.state = {
            step: 1,
            nbStep: 2,
            selectCar: false,
            allowCovoiturage: true,
            input: {
                allow_covoiturage: true,
                select_car: false,
                ville_depart: "",
                ville_arrivee: "",
                date_debut: "",
                date_fin: "",
                commentaire_trajet: "",
                commentaire_demande: "",
                voiture: 0,
                etapes: [],
                site: 0
            },
            error: {
                date_debut: false,
                date_fin: false,
                site: false
            },
            redirect: false
        };
        this.nextStep = this.nextStep.bind(this);
        this.previousStep = this.previousStep.bind(this);
        this.handleChangeSelectCar = this.handleChangeSelectCar.bind(this);
        this.handleAllowCovoiturage = this.handleAllowCovoiturage.bind(this);
        this.saveInput = this.saveInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.addEtape = this.addEtape.bind(this);
    }

    componentWillMount() {
        this.props.fetchListSiteOrga()
    }

    nextStep(){
        let nb = this.state.step + 1;
        this.setState({step: nb});
    }

    previousStep(){
        let nb = this.state.step - 1;
        this.setState({step: nb});
    }

    handleChangeSelectCar(e){
        let nbStep = this.state.selectCar ? this.state.nbStep - 1 : this.state.nbStep + 1;
        this.setState({
            selectCar: !this.state.selectCar,
            nbStep: nbStep
        });
        this.saveInput('select_car', !this.state.selectCar);
        if(this.state.input.date_debut !== "" && this.state.input.date_fin !== ""  && this.state.input.site !== 0){
            this.props.fetchDemandeVehiculeDispo(this.state.input.site, this.state.input.date_debut, this.state.input.date_fin);
        }
    }

    handleAllowCovoiturage(e){
        let nbStep = this.state.allowCovoiturage ? this.state.nbStep - 1 : this.state.nbStep + 1;
        this.setState({
            allowCovoiturage: !this.state.allowCovoiturage,
            nbStep: nbStep
        });
        this.saveInput('allow_covoiturage', !this.state.allowCovoiturage);
    }

    saveInput(name, data){
        this.setState({input : {...this.state.input, [name] : data}});
        switch (name) {
            case 'date_debut':
                data !== "" && this.setState({ error: { ...this.state.error, date_debut: false}});
                break;
            case 'date_fin':
                data !== "" && this.setState({ error: { ...this.state.error, date_fin: false}});
                break;
            case 'site':
                data !== 0 && this.setState({ error: { ...this.state.error, site: false}});
                if(this.state.input.etapes.length === 0){
                    this.setState({
                        input: {
                            ...this.state.input,
                            site: data,
                            etapes: [
                                ...this.state.input.etapes,
                                {
                                    ordre: 1,
                                    adresse: this.props.getListSite.find(o => o.sitId.toString() === data.toString()).nom
                                }
                            ]
                        }
                    });
                }else{
                    this.setState({
                        input: {
                            ...this.state.input,
                            site: data,
                            etapes: this.state.input.etapes.map((e, i) => {
                                if(i === 0){
                                    return {
                                        ordre: 1,
                                        adresse: this.props.getListSite.find(o => o.sitId.toString() === data.toString()).nom
                                    }
                                }else{
                                    return e
                                }
                            })
                        }
                    })
                }

                break;
        }
    }

    optionVehicule(){
        let option = [];
        const listSite = this.props.getDemandeVehiculeDispo;
        listSite && listSite.forEach(item => {
            option.push({
                key: item.id_vehicule,
                text: item.marque+" "+item.modele,
                value: item.id_vehicule
            })
        });
        return option;
    }

    optionSite(){
        let option = [];
        const listSite = this.props.getListSite;
        listSite && listSite.forEach(item => {
            option.push({
                key: item.sitId,
                text: item.nom,
                value: item.sitId
            })
        });
        return option;
    }

    handleSubmit(){
        if(this.state.input.date_debut === "" || this.state.input.date_fin === "" || this.state.input.site === 0){
            this.setState({error: {
                date_debut: (this.state.input.date_debut === ""),
                date_fin: (this.state.input.date_fin === ""),
                site: (this.state.input.site === 0)
            }});
        }else{
            this.props.addDemandeUtilisateur(this.props.getUtilisateurToken.token, this.state.input);
        }
    }

    addEtape(){
        this.setState({
            input: {
                ...this.state.input,
                etapes: [
                    ...this.state.input.etapes,
                    {
                        ordre: this.state.input.etapes.length + 1,
                        adresse: null
                    }
                ]
            }
        })
    }

    generateForm(){
        let html = null;
        if(this.state.step === 2 && this.state.allowCovoiturage){
            html = (
                <div style={{width: '100%'}} key="covoiturage">
                    <Grid>
                        {this.state.input.etapes.length !== 0 && (
                            <Grid.Row>
                                <Grid.Column desktop={16} computer={16} mobile={16}>
                                    {
                                        this.state.input.etapes.map((item, i) => {
                                            return (
                                                <Form.Field key={`etape-${i}`}>
                                                    <Form.Input
                                                        type="text"
                                                        label={i === 0 ? "Ville de départ" : (i === 1 ? "Etape" : "")}
                                                        name="ville_depart"
                                                        placeholder={i}
                                                        value={item.adresse !== null ? item.adresse : ""}
                                                        onChange={ (event, {value}) => {
                                                            this.setState({
                                                                input: {
                                                                    ...this.state.input,
                                                                    etapes: this.state.input.etapes.map((e, e_i) => {
                                                                        if(e_i === i){
                                                                            return {
                                                                                ordre: i +1,
                                                                                adresse: value
                                                                            }
                                                                        }else{
                                                                            return e
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }}
                                                        disabled={i === 0 && this.state.input.site !== 0}
                                                        action={{
                                                            icon: 'delete',
                                                            onClick: ((event, value) => {
                                                                const etapes = [];
                                                                this.state.input.etapes.map((item_etape, index_etape) => {
                                                                    if(index_etape !== i){
                                                                        etapes.push(item_etape);
                                                                    }
                                                                });
                                                                this.setState({
                                                                    input: {
                                                                        ...this.state.input,
                                                                        etapes: etapes
                                                                    }
                                                                });
                                                            })
                                                        }}
                                                    />
                                                </Form.Field>
                                            )
                                        })
                                    }
                                </Grid.Column>
                            </Grid.Row>
                        )}
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <a onClick={() => this.addEtape()} className="link_add_etape">Ajouter une étape</a>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <Form.TextArea
                                    label='Commentaire libre sur le trajet'
                                    name="commentaire_trajet"
                                    defaultValue={this.state.input.commentaire_trajet}
                                    onChange={ (event, {value}) => (this.saveInput('commentaire_trajet', value))}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            );
        }else if((this.state.step === 2 && !this.state.allowCovoiturage && this.state.selectCar) || (this.state.step === 3 && this.state.selectCar)){
            const optionVehicule = this.optionVehicule();
            html = (
                <div  style={{width: '100%'}} key="vehicule">
                    <Grid>
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <Form.Field>
                                    <Form.Select
                                        className="select"
                                        label="Véhicule"
                                        options={optionVehicule}
                                        onChange={ (event, {value}) => {
                                            this.saveInput('voiture', value)
                                        }}
                                        value={this.state.input.voiture}
                                    />
                                </Form.Field>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            );
        }else{
            const optionSite = this.optionSite();
            const minDate = dateFns.format(Date.now(), "YYYY-MM-DD");
            html = (
                <div style={{width: '100%'}} key="demande">
                    <Grid>
                        <Grid.Row>
                            <Grid.Column computer={8} tablet={8} mobile={16}>
                                <Form.Field>
                                    <Form.Input
                                        type="date"
                                        label="Date de début"
                                        name="date_debut"
                                        placeholder="jour/mois/année"
                                        defaultValue={this.state.input.date_debut}
                                        onChange={ (event, {value}) => (this.saveInput('date_debut', value))}
                                        error={this.state.error.date_debut}
                                        min={minDate}
                                        required
                                    />
                                </Form.Field>
                            </Grid.Column>
                            <Grid.Column computer={8} tablet={8} mobile={16}>
                                <Form.Field>
                                    <Form.Input
                                        type="date"
                                        label="Date de fin"
                                        placeholder="jour/mois/année"
                                        defaultValue={this.state.input.date_fin}
                                        onChange={ (event, {value}) => (this.saveInput('date_fin', value))}
                                        error={this.state.error.date_fin}
                                        min={minDate}
                                        required
                                    />
                                </Form.Field>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <Form.Field>
                                    <Form.Select
                                        className="select"
                                        label="Site de départ"
                                        options={optionSite}
                                        onChange={ (event, {value}) => (this.saveInput('site', value))}
                                        value={this.state.input.site}
                                        error={this.state.error.site}
                                        required
                                    />
                                </Form.Field>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <Form.Group inline>
                                    <label>Proposer du covoiturage</label>
                                    <Form.Radio
                                        label='Oui'
                                        checked={this.state.allowCovoiturage === true}
                                        onChange={((event, checked) =>  this.handleAllowCovoiturage(checked))}
                                    />
                                    <Form.Radio
                                        label='Non'
                                        checked={this.state.allowCovoiturage === false}
                                        onChange={((event, checked) =>  this.handleAllowCovoiturage(checked))}
                                    />
                                </Form.Group>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <Form.Group inline>
                                    <label>Choisir la voiture</label>
                                    <Form.Radio
                                        label='Oui'
                                        value='true'
                                        checked={this.state.selectCar === true}
                                        onChange={((event, checked) =>  this.handleChangeSelectCar(checked))}
                                    />
                                    <Form.Radio
                                        label='Non'
                                        value='false'
                                        checked={this.state.selectCar === false}
                                        onChange={((event, checked) =>  this.handleChangeSelectCar(checked))}
                                    />
                                </Form.Group>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column desktop={16}>
                                <Form.TextArea
                                    label='Commentaire libre'
                                    defaultValue={this.state.input.commentaire_demande}
                                    name="commentaire_demande"
                                    onChange={ (event, {value}) => (this.saveInput('commentaire_demande', value))}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            );
        }

        return <Fragment>{html}</Fragment>;
    }

    render(){
        const step1Classname = 'flex-start item active';
        const step2Classname = this.state.step >= 2 && this.state.allowCovoiturage ? 'flex-end item active' : 'flex-end item';
        const step3Classname = (this.state.step === 2 && !this.state.allowCovoiturage && this.state.selectCar) || (this.state.step === 3 && this.state.selectCar) ? 'flex-end item active' : 'flex-end item';
        return(
            <Fragment>
                {this.props.getDemandeIsCreate === 200 ? (
                    <Redirect to='/'/>
                ) : (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column desktop={16} computer={16} mobile={16}>
                                <Segment className="demande_add">
                                    <div className="body">
                                        <div className="flex flex-row progress">
                                            <div className={step1Classname}>
                                                <div className="flex flex-column flex-center">
                                                    <div className="icon"><FontAwesomeIcon icon="calendar-alt"/></div>
                                                    <div className="text">Réservation</div>
                                                </div>
                                            </div>
                                            {this.state.allowCovoiturage && (
                                                <div className={step2Classname}>
                                                    <div className="flex flex-column flex-center">
                                                        <div className="icon"><FontAwesomeIcon icon="users"/></div>
                                                        <div className="text">Covoiturage</div>
                                                    </div>
                                                </div>
                                            )}
                                            {this.state.selectCar && (
                                                <div className={step3Classname}>
                                                    <div className="flex flex-column flex-center">
                                                        <div className="icon"><FontAwesomeIcon icon="car"/></div>
                                                        <div className="text">Véhicule</div>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                        <Form>
                                            <Grid>
                                                {this.generateForm()}
                                                <Grid.Row>
                                                    <Grid.Column desktop={16} computer={16} mobile={16}>
                                                        <div className="flex flex-row">
                                                            {this.state.step > 1 ? (
                                                                <div className="flex-start">
                                                                    <Button onClick={this.previousStep}>Précédent</Button>
                                                                </div>
                                                            ) : ('')}
                                                            <div className="flex-end">
                                                                {this.state.step < this.state.nbStep ? (
                                                                    <Button onClick={this.nextStep}>Suivant</Button>
                                                                ) : (
                                                                    <Button onClick={() => this.handleSubmit()}>Valider</Button>
                                                                )}
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Form>
                                    </div>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                )}
            </Fragment>
        )
    }
}