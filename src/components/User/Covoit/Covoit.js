import {connect} from "react-redux";
import Covoit from './Covoit.jsx';
import {getListeCovoit} from "../../redux/reducers/demandeUtilisateur";
import {fetchListeCovoit} from "../../redux/actions/demandeUtilisateur";

const mapStateToProps = (state) => {
    return {
        getListeCovoit : getListeCovoit(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchListeCovoit: () => dispatch(fetchListeCovoit()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Covoit);