import React, { Component } from 'react';
import {Label, Button, Segment, Grid, Modal} from "semantic-ui-react";
import { Table, Thead, Tbody, Tr, Th, Td } from "react-super-responsive-table"
import "react-super-responsive-table/dist/SuperResponsiveTableStyle.css"
import DetailsDemande from "../DetailsDemande/DetailsDemande";

export default class Demande extends Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.fetchListeCovoit();
    }

    render(){
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column desktop={16} computer={16} mobile={16}>
                        <Segment>
                            <div className="header colored">
                                <div className="flex flex-row">
                                    <p className="title flex-start">
                                        Liste des covoiturages disponible
                                    </p>
                                </div>
                            </div>
                            <div className="body">
                                <Table>
                                    <Thead>
                                        <Tr>
                                            <Th>Lieu du départ</Th>
                                            <Th>Date du départ</Th>
                                            <Th>Date d'arrivée</Th>
                                            <Th>Etapes</Th>
                                            <Th>Véhicule</Th>
                                            <Th>Collaborateur</Th>
                                            <Th>Téléphone</Th>
                                            <Th>Email</Th>
                                        </Tr>
                                    </Thead>
                                    <Tbody>
                                        {this.props.getListeCovoit.map((item, i) => {
                                            return (
                                                <Tr key={i}>
                                                    <Td>{item.nomSite}</Td>
                                                    <Td>{item.dateDebut}</Td>
                                                    <Td>{item.dateFin}</Td>
                                                    <Td>{item.etapes !== null && (
                                                        <ol>
                                                            {
                                                                item.etapes.map((e, i) => {
                                                                    return (<li>{e.adresse}</li>)
                                                                })
                                                            }
                                                        </ol>
                                                    )}</Td>
                                                    <Td>{item.vehicule !== null && item.vehicule.marque+" "+item.vehicule.modele}</Td>
                                                    <Td>{item.nomUtilisateur.toUpperCase()} {item.prenomUtilisateur}</Td>
                                                    <Td>{item.numeroTelephone}</Td>
                                                    <Td>{item.mailUtilisateur}</Td>
                                                </Tr>
                                            )
                                        })}
                                    </Tbody>
                                </Table>
                            </div>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}