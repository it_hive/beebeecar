import {connect} from "react-redux";
import Demande from './Demande.jsx';
import {getUtilisateurToken} from "../../redux/reducers/connexion";
import {getDemandeUtilisateurAttente} from "../../redux/reducers/demandeUtilisateur";
import {fetchDemandeUtilisateurAttente} from "../../redux/actions/demandeUtilisateur";

const mapStateToProps = (state) => {
    return {
        demandeAttente : getDemandeUtilisateurAttente(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDemandeUtilisateurAttente: () => dispatch(fetchDemandeUtilisateurAttente()),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Demande);