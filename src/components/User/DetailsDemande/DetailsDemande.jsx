import React, { Component, Fragment } from 'react';
import {Button, Grid, Form} from "semantic-ui-react";
import "react-super-responsive-table/dist/SuperResponsiveTableStyle.css"
let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../translate/fr.yml'));

export default class DetailsDemande extends Component{
    constructor(props){
        super(props);
    }

    generateEtape(demande){
        const ol_etapes = [];

        function compare(a, b){
            // Use toUpperCase() to ignore character casing
            const genreA = a.ordre;
            const genreB = b.ordre;

            let comparison = 0;
            if (genreA > genreB) {
                comparison = 1;
            } else if (genreA < genreB) {
                comparison = -1;
            }
            return comparison;
        }

        if(demande.etapes !== null && demande.etapes.length !== 0){
            const etapes = demande.etapes.sort(compare);
            etapes.map((item, i) => {
                ol_etapes.push(
                    <li key={i} style={{paddingLeft: '5px'}}>{item.adresse}</li>
                )
            });

            return <ol style={{marginTop: '0', paddingLeft: '25px'}}>{ol_etapes}</ol>;
        }

        return null
    }

    render(){
        const demande = this.props.demande;
        return(
            <Grid>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <p><strong>Numéro :</strong> {demande.demId}</p>
                        <p><strong>Date de la demande :</strong> {demande.dateDemande}</p>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <p><strong>Date de début :</strong> {demande.dateDebut}</p>
                        <p><strong>Date de fin :</strong> {demande.dateFin}</p>
                        {(demande.commentaire !== null && demande.commentaire !== "") && (
                            <p><strong>Commentaire :</strong> {demande.commentaire}</p>
                        )}
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <p><strong>Lieu de départ :</strong> {demande.nomSite} </p>
                        <p><strong>Voiture : </strong>
                        {demande.vehicule !== null ? (
                             demande.vehicule.marque+" "+demande.vehicule.modele+" - "+demande.vehicule.immatriculation
                        ) : "Non sélectionné"}
                        </p>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <p><strong>Autorise le covoiturage :</strong> {demande.allowCovoiturage?T.translate("app.global.yes"):T.translate("app.global.no")}</p>
                        {(demande.etapes !== null && demande.etapes.length !== 0) && (
                            <Fragment>
                                <p style={{marginBottom: '5px'}}><strong>Etapes :</strong></p>
                                {this.generateEtape(demande)}
                            </Fragment>
                        )}
                        {(demande.commentaireCovoit !== null && demande.commentaireCovoit !== "") && (
                            <p><strong>Commentaire :</strong> {demande.commentaireCovoit}</p>
                        )}
                    </Grid.Column>
                </Grid.Row>
                {demande.canValidate && (
                    <Grid.Row>
                        <div className="flex flex-row">
                            <div className="flex-center">
                                <Button color='red' onClick={() => {this.props.refuserDemandeLocation(demande.demId)}}>Refuser</Button>
                                <Button onClick={() => {this.validerDemande()}}>Valider</Button>
                            </div>
                        </div>
                    </Grid.Row>
                )}
            </Grid>
        )
    }
}