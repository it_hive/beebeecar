import {connect} from "react-redux";
import FloatingButton from './FloatingButton.jsx';
import {setMenuChoice} from "./../../redux/actions/menu";

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuChoice: (item) => dispatch(setMenuChoice(item))
    }
}

export default connect(null, mapDispatchToProps)(FloatingButton);