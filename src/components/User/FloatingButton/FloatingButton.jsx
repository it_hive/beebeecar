import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

export default class FloatingButton extends Component{
    constructor(props){
        super(props);
        this.changeMenu = this.changeMenu.bind(this);
    }

    changeMenu(){
        this.props.setMenuChoice("new");
    }

    render(){
        return (
            <Link to={'/new'} className="floatingButton" onClick={this.changeMenu}>
                <p className="add">
                    <FontAwesomeIcon icon="plus"/>
                </p>
            </Link>
        )
    }
}