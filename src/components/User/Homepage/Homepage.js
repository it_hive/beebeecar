import {connect} from "react-redux";
import Homepage from './Homepage.jsx';
import {setMenuChoice} from "../../redux/actions/menu";
import {getUserInfos} from "../../redux/reducers/connexion";
import {getDemandeIsCreate} from "../../redux/reducers/demandeUtilisateur";
import {setDemandeIsCreate} from "../../redux/actions/demandeUtilisateur";

const mapStateToProps = (state) => {
    return {
        userInfos : getUserInfos(state),
        getDemandeIsCreate : getDemandeIsCreate(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuChoice: (item) => dispatch(setMenuChoice(item)),
        setDemandeIsCreate: (item) => dispatch(setDemandeIsCreate(item))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Homepage);