import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {Segment, Button, Grid, Container, Table, Message} from 'semantic-ui-react';
import LastDemande from "./LastDemande/LastDemande";
import LastReservation from "./LastReservation/LastReservation";
import {getDemandeIsCreate} from "../../redux/reducers/demandeUtilisateur";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../translate/fr.yml'));

export default class Homepage extends Component{
    constructor(props){
        super(props);
        this.state = {
            message: true
        };
        this.onClickAction = this.onClickAction.bind(this);
    }

    onClickAction(link){
        this.props.setMenuChoice(link);
    }

    componentWillMount() {
        if((this.props.getDemandeIsCreate === 200)){
            this.setState({ message: false });
            setTimeout(() => {
                this.setState({ message: true });
                this.props.setDemandeIsCreate(null);
            }, 3000);
        }
    }

    render(){
        return (
            <Fragment>
                <Grid>
                    <Grid.Row className="home_message">
                        <Grid.Column computer={16} mobile={16}>
                            <Message
                                positive
                                hidden = {this.state.message}
                                onDismiss={this.handleDismiss}
                                header={this.props.getDemandeIsCreate === 200 && T.translate("app.demande.isCreate")}
                                content={this.props.getDemandeIsCreate === 200 && T.translate("app.demande.isCreateLong")}
                            />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column computer={16} mobile={16}>
                            <Segment>
                                <div className="header colored">
                                    <p className="title">Mes demandes en attente</p>
                                    <div className="button">
                                        <Link to={"/demande"} onClick={() => this.onClickAction("demande")}>
                                            <Button>Voir tout</Button>
                                        </Link>
                                    </div>
                                </div>
                                <div className="body">
                                    <LastDemande utilisateur={this.props.userInfos}/>
                                </div>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column computer={16} mobile={16}>
                            <Segment>
                                <div className="header colored">
                                    <p className="title">Mes demandes validées</p>
                                    <div className="button">
                                        <Link to={"/reservation"} onClick={() => this.onClickAction("reservation")}>
                                            <Button>Voir tout</Button>
                                        </Link>
                                    </div>
                                </div>
                                <div className="body">
                                    <LastReservation utilisateur={this.props.userInfos}/>
                                </div>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Fragment>
        )
    }
}