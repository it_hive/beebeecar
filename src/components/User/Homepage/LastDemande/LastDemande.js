import {connect} from "react-redux";
import LastDemande from './LastDemande.jsx';
import {fetchDemandeUtilisateurAttente} from "../../../redux/actions/demandeUtilisateur";
import {getDemandeUtilisateurAttente} from "../../../redux/reducers/demandeUtilisateur";
import {getUtilisateurToken} from "../../../redux/reducers/connexion";

const mapStateToProps = (state) => {
    return {
        demandeAttente : getDemandeUtilisateurAttente(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDemandeUtilisateurAttente: () => dispatch(fetchDemandeUtilisateurAttente()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LastDemande);