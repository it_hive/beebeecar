import React, { Component, Fragment } from 'react';
import {Button, Label, Modal} from 'semantic-ui-react';
import {Table, Tbody, Td, Th, Thead, Tr} from "react-super-responsive-table";
import DetailsDemande from "../../DetailsDemande/DetailsDemande";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../../translate/fr.yml'));

export default class LastDemande extends Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.fetchDemandeUtilisateurAttente();
    }

    render(){
        return (
            <Table>
                <Thead>
                    <Tr>
                        <Th>Date de la demande</Th>
                        <Th>Date du départ</Th>
                        <Th>Date d'arrivée</Th>
                        <Th>Véhicule</Th>
                        <Th>Covoiturage</Th>
                        <Th/>
                    </Tr>
                </Thead>
                <Tbody>
                    {this.props.demandeAttente.map((item, i) => {
                        return (
                            (i < 4 && (
                                <Tr key={i}>
                                    <Td>{item.dateDemande}</Td>
                                    <Td>{item.dateDebut}</Td>
                                    <Td>{item.dateFin}</Td>
                                    <Td>{item.vehicule !== null && item.vehicule.marque+" "+item.vehicule.modele}</Td>
                                    <Td>{item.allowCovoiturage === 0 ? 'Non' : 'Oui'}</Td>
                                    <Td>
                                        <Modal
                                            trigger={<Button key={item.demId}>Détails</Button>}
                                        >
                                            <Modal.Header>Détails de la demande</Modal.Header>
                                            <Modal.Content>
                                                <Modal.Description>
                                                    <DetailsDemande demande={item} key={item.demId}/>
                                                </Modal.Description>
                                            </Modal.Content>
                                        </Modal>
                                    </Td>
                                </Tr>
                            ))
                        )
                    })}
                </Tbody>
            </Table>
        )
    }
}