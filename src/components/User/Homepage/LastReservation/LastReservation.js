import {connect} from "react-redux";
import LastReservation from './LastReservation.jsx';
import {getDemandeUtilisateur} from "../../../redux/reducers/demandeUtilisateur";
import {fetchDemandeUtilisateur} from "../../../redux/actions/demandeUtilisateur";
import {getUtilisateurToken} from "../../../redux/reducers/connexion";

const mapStateToProps = (state) => {
    return {
        getUtilisateurToken: getUtilisateurToken(state).token,
        getDemande : getDemandeUtilisateur(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDemandeUtilisateur: () => dispatch(fetchDemandeUtilisateur()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LastReservation);