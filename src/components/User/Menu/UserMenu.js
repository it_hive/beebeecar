import {connect} from "react-redux";
import UserMenu from './UserMenu.jsx';

import {setOpenMenu} from "./../../redux/actions/menu";
import {getMenuChoice, getOpenMenu} from "./../../redux/reducers/menu";
import {disconnect} from "../../redux/actions/connexion";

const mapStateToProps = (state) => {
    return {
        getMenuChoice : getMenuChoice(state),
        getOpenMenu: getOpenMenu(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deconnexion: () => dispatch(disconnect()),
        setOpenMenu: (value) => dispatch(setOpenMenu(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserMenu);