import React, { Component, Fragment } from 'react';
import UserMenuButton from "./UserMenuButton";
import Logo from "../../../../public/images/Logo.png";
import LogoLigne from "../../../../public/images/LogoGaucheBlanc.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

let T = require('i18n-react/dist/i18n-react').default;
T.setTexts(require('json-loader!yaml-loader!../../../translate/fr.yml'));

export default class UserMenu extends Component{

    constructor(props){
        super(props);
        this.state = {
            open: false
        }
        this.onOpenMenu = this.onOpenMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
        this.disconnect = this.disconnect.bind(this);
    }

    onOpenMenu(){
        if(this.props.getOpenMenu === true){
            this.props.setOpenMenu(false);
        }else{
            this.props.setOpenMenu(true);
        }
    }

    closeMenu(){
        this.props.setOpenMenu(false);
    }

    disconnect(){
        this.props.deconnexion()
    }

    render(){
        return (
            <Fragment>
                <div className="nav">
                    <h2 className="nav-brand">
                        <a href="#" className="nav-logo">
                            <img src={LogoLigne} alt=""/>
                        </a>
                    </h2>
                    <ul className="nav-menu">
                        <UserMenuButton
                            label={T.translate("app.menu.home")}
                            link=''
                            isDesktop="true"
                            icon="home"
                            active={"dashboard" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label={T.translate("app.menu.demandes")}
                            link='demande'
                            isDesktop="true"
                            icon="clipboard-list"
                            active={"demande" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label={T.translate("app.menu.reservations")}
                            link='reservation'
                            isDesktop="true"
                            icon="calendar"
                            active={"reservation" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label="Covoiturage"
                            link='covoiturage'
                            isDesktop="true"
                            icon="users"
                            active={"covoiturage" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label={T.translate("app.menu.compte")}
                            link='profil'
                            isDesktop="true"
                            icon="user"
                            active={"profil" === this.props.getMenuChoice}
                        />
                        <li className="nav-menu-item">
                            <a href="#" className="nav-menu-link" onClick={this.disconnect}>
                                <FontAwesomeIcon icon="sign-out-alt"/> {T.translate("app.menu.disconnect")}
                            </a>
                        </li>
                    </ul>
                    <div className={this.props.getOpenMenu ? 'hamburger-icon active' : 'hamburger-icon'} id="hamburger-icon" onClick={this.onOpenMenu}>
                        <span className="hamburger-line" />
                        <span className="hamburger-line" />
                        <span className="hamburger-line" />
                    </div>
                </div>
                <div id="side-nav" className={this.props.getOpenMenu ? 'side-nav open' : 'side-nav'}>
                    <ul className="side-nav-menu">
                        <UserMenuButton
                            label={T.translate("app.menu.home")}
                            link=''
                            isDesktop="false"
                            icon=""
                            active={"home" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label={T.translate("app.menu.demandes")}
                            link='demande'
                            isDesktop="false"
                            icon=""
                            active={"demande" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label={T.translate("app.menu.reservations")}
                            link='reservation'
                            isDesktop="false"
                            icon=""
                            active={"reservation" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label="Covoiturage"
                            link='covoiturage'
                            isDesktop="false"
                            icon=""
                            active={"covoiturage" === this.props.getMenuChoice}
                        />
                        <UserMenuButton
                            label={T.translate("app.menu.profil")}
                            link='profil'
                            isDesktop="false"
                            icon=""
                            active={"profil" === this.props.getMenuChoice}
                        />
                        <li className="side-nav-item">
                            <a href="#" className="side-nav-link" onClick={this.disconnect}>
                                {T.translate("app.menu.disconnect")}
                            </a>
                        </li>
                    </ul>
                </div>
            </Fragment>
        );
    }
}