import {connect} from "react-redux";
import MenuButton from './UserMenuButton.jsx';

import {setMenuChoice, setOpenMenu} from "./../../redux/actions/menu";

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuChoice: (item) => dispatch(setMenuChoice(item)),
        setOpenMenu: (item) => dispatch(setOpenMenu(item))
    }
}

export default connect(null, mapDispatchToProps)(MenuButton);