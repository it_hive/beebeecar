import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class UserMenuButton extends Component{

    constructor(props){
        super(props);
        this.state = {
            isActive: false,
            isDesktop: this.props.isDesktop,
            label: this.props.label,
            link: this.props.label
        }

        this.onClickAction = this.onClickAction.bind(this);
    }

    onClickAction(){
        const link = (this.props.link !== "") ? this.props.link : "dashboard";
        this.props.setMenuChoice(link);
        this.props.setOpenMenu(false);
    }

    render(){
        const liClassName = this.props.isDesktop === "true" ? (this.props.active ? 'nav-menu-item active' : 'nav-menu-item') : 'side-nav-item';
        const linkClassName = this.props.isDesktop === "true" ? (this.props.active ? 'nav-menu-link active' : 'nav-menu-link') : (this.props.active ? 'side-nav-link active' : 'side-nav-link');
        return (
            <li className={liClassName}>
                <Link to={'/'+this.props.link} className={linkClassName} onClick={this.onClickAction}>
                    {this.props.icon !== "" && <FontAwesomeIcon icon={this.props.icon}/>} {this.state.label}
                </Link>
            </li>
        );
    }
}