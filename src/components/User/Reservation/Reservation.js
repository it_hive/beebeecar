import {connect} from "react-redux";
import Reservation from './Reservation.jsx';
import {getDemandeUtilisateur} from "../../redux/reducers/demandeUtilisateur";
import {fetchDemandeUtilisateur} from "../../redux/actions/demandeUtilisateur";
import {getUserInfos, getUtilisateurToken} from "../../redux/reducers/connexion";

const mapStateToProps = (state) => {
    return {
        getUtilisateurToken: getUtilisateurToken(state).token,
        getDemande : getDemandeUtilisateur(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDemandeUtilisateur: (idUtilisateur) => dispatch(fetchDemandeUtilisateur(idUtilisateur)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Reservation);