import React, { Component } from 'react';
import {Button, Grid, Label, Modal, Segment} from "semantic-ui-react";
import { Table, Thead, Tbody, Tr, Th, Td } from "react-super-responsive-table"
import DetailsDemande from "../DetailsDemande/DetailsDemande";

export default class Reservation extends Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.fetchDemandeUtilisateur(this.props.getUtilisateurToken);
    }

    render(){
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column desktop={16} computer={16} mobile={16}>
                        <Segment>
                            <div className="header colored">
                                <div className="flex flex-row">
                                    <p className="title flex-start">
                                        Mes réservations
                                    </p>
                                </div>
                            </div>
                            <div className="body">
                                <Table>
                                    <Thead>
                                        <Tr>
                                            <Th>Date de la demande</Th>
                                            <Th>Date du départ</Th>
                                            <Th>Date d'arrivée</Th>
                                            <Th>Véhicule</Th>
                                            <Th>Covoiturage</Th>
                                            <Th/>
                                        </Tr>
                                    </Thead>
                                    <Tbody>
                                        {this.props.getDemande.map((item, i) => {
                                            return (
                                                <Tr key={i}>
                                                    <Td>{item.dateDemande}</Td>
                                                    <Td>{item.dateDebut}</Td>
                                                    <Td>{item.dateFin}</Td>
                                                    <Td>{item.vehicule.marque} {item.vehicule.modele}</Td>
                                                    <Td>{item.allowCovoiturage === 0 ? 'Non' : 'Oui'}</Td>
                                                    <Td>
                                                        <Modal
                                                            trigger={<Button key={item.demId}>Détails</Button>}
                                                        >
                                                            <Modal.Header>Détails de la demande</Modal.Header>
                                                            <Modal.Content>
                                                                <Modal.Description>
                                                                    <DetailsDemande demande={item} key={item.demId}/>
                                                                </Modal.Description>
                                                            </Modal.Content>
                                                        </Modal>
                                                    </Td>
                                                </Tr>
                                            )
                                        })}
                                    </Tbody>
                                </Table>
                            </div>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}