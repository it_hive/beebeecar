import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

export default class HiveBar extends Component{

    render(){

        var data = {
            datasets: [{
                data: this.props.data,
                backgroundColor:[
                    "rgba(37, 48, 76, 0.5)",
                    "rgba(255, 213, 43, 0.5)",
                    "rgba(198, 255, 255, 0.5)",
                    "rgba(15, 15, 15, 0.5)"
                ]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'S7',
                'S8',
                'S9',
                'S10'
            ]
        };

        let options = {
            legend: {
                position: 'none'
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            aspectRation: 1
        };

        return (
            <Bar
                data= {data}
                height={null}
                width={null}
                options={options}
            />
        );
    }
}