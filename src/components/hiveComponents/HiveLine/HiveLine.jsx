import React, { Component } from 'react';
import {Bar, Line} from 'react-chartjs-2';

export default class HiveLine extends Component{

    render(){

        var data = {
            datasets: [{
                data: this.props.data
            }],
            labels: [
                'S7',
                'S8',
                'S9',
                'S10'
            ],
            backgroundColor: [
                "green"
            ]
        };

        let options = {
            legend: {
                position: 'none'
            },
            steppedLine: true,
            scales: {
                yAxes: [{
                    stacked: true
                }]
            },
            aspectRation: 1
        };

        return (
            <Line
                data= {data}
                height={null}
                width={null}
                options={options}
            />
        );
    }
}