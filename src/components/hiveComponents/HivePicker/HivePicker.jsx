import React, {Component} from 'react';
import { Popup } from 'semantic-ui-react'
import reactCSS from 'reactcss';
import { CustomPicker, CirclePicker } from 'react-color';

class HivePicker extends Component{

    constructor(props){
        super(props);
        this.state = {
            displayColorPicker: null,
            color: {
                r: '255',
                g: '192',
                b: '56',
                a: '1',
            },
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClick(){
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose(){
        this.setState({ displayColorPicker: false })
    };

    handleChange(color){
        this.setState({ color: color.rgb })
        this.props.handleChange(color.hex)
    };

    componentWillMount(){
        if(this.props.defaultValue)
            this.setState({color: this.props.defaultValue.rgb});
    }

    render(){
        const styles = reactCSS({
            'default': {
                color: {
                    width: '100%',
                    minHeight: '33px',
                    borderRadius: '.28571429rem',
                    background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
                },
                swatch: {
                    width: '100%',
                    minHeight: '38px',
                    padding: '5px',
                    background: '#fff',
                    borderRadius: '.28571429rem',
                    boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
                    display: 'inline-block',
                    cursor: 'pointer'
                }
            },
        });

        const colors = ['#D0021B', '#F5A623', '#F8E71C', '#8B572A', '#7ED321', '#417505', '#BD10E0', '#9013FE', '#4A90E2', '#50E3C2', '#B8E986', '#000000', '#4A4A4A', '#9B9B9B', '#FFFFFF'];

        return (
            <Popup
                trigger={
                    <div style={ styles.swatch }>
                        <div style={ styles.color } />
                    </div>}
                content={
                    <CirclePicker
                        colors={colors}
                        color={ this.props.color }
                        onChange={ this.handleChange }
                    />
                }
                on='click'
            />
        );
    }
}

export default CustomPicker(HivePicker);