import React, { Component } from 'react';
import {Bar, Pie} from 'react-chartjs-2';

export default class HivePie extends Component{

    render(){

        var data = {
            datasets: [{
                data: this.props.data,
                backgroundColor:[
                    "rgba(46, 204, 113 , 0.5)",
                    "rgba(241, 196, 15, 0.5)",
                    "rgba(231, 76, 60, 0.5)"
                ],
                borderColor: [
                    "white"
                ]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Réservée',
                'Disponible',
                'Indisponible'
            ]
        };

        let options = {
            legend: {
                position: 'bottom'
            },
            aspectRation: 1
        };

        return (
            <Pie
                data= {data}
                height={null}
                width={null}
                options={options}
            />
        );
    }
}