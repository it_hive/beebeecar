import React, {Component} from 'react';
import {Bar, Radar} from 'react-chartjs-2';

export default class HiveRadar extends Component {

    constructor() {
        super();
        this.getRandomArbitrary = this.getRandomArbitrary.bind(this);
    }

    getRandomArbitrary() {
        return Math.random() * 255;
    }

    render() {
        var dataArray = [];

        this.props.data && this.props.data.forEach(item => {
            let color1 = this.getRandomArbitrary();
            let color2 = this.getRandomArbitrary();
            let color3 = this.getRandomArbitrary();

            dataArray.push({
                ...item,
                backgroundColor: [`rgba(${color1}, ${color2}, ${color3}, 0.1`],
                borderColor: [`rgba(${color1}, ${color2}, ${color3}, 0.5`]
            })
        });

        var data = {
            datasets: dataArray,

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Jour',
                'Semaine',
                'Mois',
                'Année'
            ]
        };

        let options = {
            legend: {
                position: 'bottom'
            },
            aspectRation: 1
        };

        return (
            <Radar
                data={data}
                height={null}
                width={null}
                options={options}
            />
        );
    }
}