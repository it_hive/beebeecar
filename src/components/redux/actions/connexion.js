import config from './../../../config';
import {getJson} from "../../../utils/http";

export const SET_AUTH = "SET_AUTH";
export const SET_AUTH_ERROR = "SET_AUTH_ERROR";
export const SET_DISCONNECT = "SET_DISCONNECT";
export const SET_CODE_RETOUR = "SET_CODE_RETOUR";
export const SET_USER_INFO = "SET_USER_INFO";

export const setAuthenticate = (donnee) => {
    return { type: SET_AUTH, donnee}
};

export function disconnect(){
    return { type: SET_DISCONNECT}
}

export function setAuthError(donnee){
    return { type: SET_AUTH_ERROR, donnee};
};

export function setUserConnect(donnee){
    return { type: SET_USER_INFO, donnee}
}

export function seConnecter(login, password){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'utilisateur/connexionToken/',{
            method: 'POST',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'mailUtilisateur' : login,
                'motDePasse' : password
            })
        })
        .then(response => getJson(response))
        .then(result => {
            if(result.code !== undefined && result.code === 201){
                dispatch(setAuthError("Utilisateur inconnu."));
            }else if(result.code !== undefined && result.code === 202 ){
                dispatch(setAuthError("Identifiant ou mot de passe incorect."));
            }else if(result.code !== undefined && result.code === 206){
                dispatch(setAuthError("Utilisateur archivé."));
            } else{
                dispatch(setAuthenticate(result));
                dispatch(fetchInfosUtilisateur(result.token));
            }
        })
        .catch((e) => {
            dispatch(setAuthError("Connexion au serveur impossible."));
        })
    }
}

export function fetchInfosUtilisateur(token){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'utilisateur/utilisateurConnecte/',{
            method: 'GET',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json',
                'token': token
            }
        }).then(response => getJson(response))
        .then(result => {
            dispatch(setUserConnect(result));
        })
        .catch((e) => {
        })
    }
}
