import config from './../../../config';
import {getJson} from "../../../utils/http";
import {formatDateToString} from "../../../utils/date";
import {getUtilisateurToken} from "../reducers/connexion";

export const SET_DEMANDES_LOCATIONS = "SET_DEMANDES_LOCATIONS";
export const SET_DEMANDES_LOCATIONS_DETAILS = "SET_DEMANDES_LOCATIONS_DETAILS";
export const SET_DEMANDES_JOUR = "SET_DEMANDES_JOUR";
export const SET_DEMANDE_VALIDATE = "SET_DEMANDE_VALIDATE";
export const SET_DEMANDE_REFUSE = "SET_DEMANDE_REFUSE";

export const setDemandes = (result) => {
    return { type: SET_DEMANDES_LOCATIONS, result}
};

export const setDemandesDetails = (result) => {
    return { type: SET_DEMANDES_LOCATIONS_DETAILS, result}
};

export const setDemandesJour = (donnee) => {
    return {type: SET_DEMANDES_JOUR, donnee}
};

export const setDemandeValidate = (donnee) => {
    return {type: SET_DEMANDE_VALIDATE, donnee}
};

export const setDemandeRefuse = (donnee) => {
    return {type: SET_DEMANDE_REFUSE, donnee}
};

export function fetchDemandeLocation(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'demandeLocation/'+getState().auth.token.token+'/EnCoursDeValidation/true',{
            method: 'GET',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            }
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setDemandes(result));
            })
            .catch((e) => {
            })
    }
}

export function fetchDemandeLocationDetails(idDemande){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'demandeLocation/get/'+idDemande,{
            method: 'GET',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            }
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setDemandesDetails(result));
            })
            .catch((e) => {
            })
    }
}

export function validerDemandeLocation(idUtilisateur, idDemande, idVehicule){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'demandeLocation/validate/'+getState().auth.token.token,{
            method: 'PUT',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "idDemande": idDemande,
                "idVehicule": idVehicule
            })
        })
            .then(response => {
                dispatch(setDemandeValidate(response.status));
                dispatch(fetchMailEtatDemande(idUtilisateur, idDemande));
                getJson(response)
            })
            .then(result => {
                dispatch(fetchDemandeLocation());
            })
            .catch((e) => {
            })
    }
}

export function refuserDemandeLocation(idUtilisateur, idDemande){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'demandeLocation/refuse/'+getState().auth.token.token,{
            method: 'PUT',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "idDemande": idDemande,
            })
        })
            .then(response => {
                dispatch(setDemandeRefuse(response.status));
                dispatch(fetchMailEtatDemande(idUtilisateur, idDemande));
                getJson(response)
            })
            .then(result => {
                dispatch(fetchDemandeLocation());
            })
            .catch((e) => {
            })
    }
}

export function fetchDemandesJour(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'demandeLocatio/getTodayValidated/'+getState().auth.token.token,{
            method: 'GET',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            }
        })
        .then(response => getJson(response))
        .then(result => {
            dispatch(setDemandesJour(result));
        })
        .catch((e) => {
        })
    }
}

function fetchMailEtatDemande(idUtilisateur, demandeLocationId) {
    return (dispatch, getState) => {
        fetch(config['base_url'] +`demandeLocationStatut/email/${idUtilisateur}/${demandeLocationId}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then()
        .catch()
    }
}