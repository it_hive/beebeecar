import config from './../../../config';
import {getJson} from "../../../utils/http";
import dateFns from "date-fns";
var fr = require('date-fns/locale/fr'); // Import french translation for date

export const SET_DEMANDE_UTILISATEUR_ATTENTE = "SET_DEMANDE_UTILISATEUR_ATTENTE";
export const SET_DEMANDE_UTILISATEUR = "SET_DEMANDE_UTILISATEUR";
export const SET_DEMANDE_VEHICULE_DISPO = "SET_DEMANDE_VEHICULE_DISPO";
export const SET_DEMANDE_IS_CREATE = "SET_DEMANDE_IS_CREATE";
export const SET_LISTE_COVOIT = "SET_LISTE_COVOIT";

export const setDemandeUtilisateurAttente = (donnee) => {
    return { type: SET_DEMANDE_UTILISATEUR_ATTENTE, donnee}
};

export const setDemandeUtilisateur = (donnee) => {
    return { type: SET_DEMANDE_UTILISATEUR, donnee}
};

export const setDemandeVehiculeDispo = (donnee) => {
    return { type: SET_DEMANDE_VEHICULE_DISPO, donnee}
};

export const setDemandeIsCreate = (donnee) => {
    return { type: SET_DEMANDE_IS_CREATE, donnee}
};

export const setListeCovoit = (donnee) => {
    return {type: SET_LISTE_COVOIT, donnee}
};

export function fetchDemandeUtilisateurAttente(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`demandeLocation/${getState().auth.token.token}/EnCoursDeValidation/true`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setDemandeUtilisateurAttente(result));
            })
            .catch((e) => {
            })
    }
}

export function fetchDemandeUtilisateur(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`demandeLocation/${getState().auth.token.token}/Valide/true`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setDemandeUtilisateur(result));
            })
            .catch((e) => {
            })
    }
}

export function fetchDemandeVehiculeDispo(idSite, dateDebut, dateFin){
    let debut = dateDebut.split('-');
    const fin = dateFin.split('-');

    return (dispatch, getState) => {
        fetch(config['base_url'] +`voiture/vehiculeDispos/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'dateDebut' : debut[2]+"/"+debut[1]+"/"+debut[0],
                'dateFin' : fin[2]+"/"+fin[1]+"/"+fin[0],
                'idSite': idSite
            })
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setDemandeVehiculeDispo(result));
            })
            .catch((e) => {
            })
    }
}

export function addDemandeUtilisateur(token, input){
    const debut = input.date_debut.split('-');
    const fin = input.date_fin.split('-');
    return (dispatch) => {
        fetch(config['base_url'] +`demandeLocation/new/${token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "allowCovoiturage": input.allow_covoiturage === true ? 1 : 0,
                "commentaireCovoiturage": input.commentaire_demande,
                "commentaireLibre": input.commentaire_trajet,
                "dateArrivee": fin[2]+"/"+fin[1]+"/"+fin[0],
                "dateDepart": debut[2]+"/"+debut[1]+"/"+debut[0],
                "etapes": input.etapes,
                "idSite": input.site,
                "idVehicule": input.voiture
            })
        })
            .then(response => {
                const statusCode = response.status;
                const data = response.json();
                return Promise.all([statusCode, data]);
            })
            .then(response => {
                dispatch(setDemandeIsCreate(response[0]));
                dispatch(fetchDemandeUtilisateurAttente());
                dispatch(fetchDemandeUtilisateur());
                dispatch(sendDemandeEmail(response[1].idUtilisateur, response[1].demId))
            })

        .catch((e) => {
            //console
        })
    }
}

export function sendDemandeEmail(idUtilisateur, idDemande){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`demandeLocation/email/${idUtilisateur}/${idDemande}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then()
            .catch()
    }
}

export function fetchListeCovoit(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`demandeLocation/covoiturages/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'dateReference' : dateFns.format(Date.now(), "DD/MM/YYYY", {locale: fr})
            })
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setListeCovoit(result));
            })
            .catch((e) => {
            })
    }
}