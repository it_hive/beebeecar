export const SET_MENU_CHOICE = "SET_MENU_CHOICE";
export const SET_OPEN_MENU = "SET_OPEN_MENU";
export const SET_OPEN_MENU_DASHBOARD = "SET_OPEN_MENU_DASHBOARD";

export const setMenuChoice = (donnee) => {
    return { type: SET_MENU_CHOICE, donnee}
}

export const setOpenMenu = (donnee) => {
    return { type: SET_OPEN_MENU, donnee}
}

export const setOpenMenuDashboard = (donnee) => {
    return { type: SET_OPEN_MENU_DASHBOARD, donnee}
}