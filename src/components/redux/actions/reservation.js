import config from './../../../config';
import {getJson} from "../../../utils/http";
import { formatToTimeZone } from "date-fns-timezone"
import {getUtilisateurToken} from "../reducers/connexion";

export const SET_LIST_RESERVATION = "SET_LIST_RESERVATION";

export const setReservation = (result) => {
    return { type: SET_LIST_RESERVATION, result}
}

export default function fetchReservation(dateDebut, dateFin, idOrganisme){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`planning/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                'dateDeb' : formatToTimeZone(dateDebut, "DD/MM/YYYY", { timeZone: 'Europe/Paris' }),
                'dateFin' : formatToTimeZone(dateFin, "DD/MM/YYYY", { timeZone: 'Europe/Paris' }),
                'idOrg' : idOrganisme
            })
        }).then(response => getJson(response))
            .then(result => {
                dispatch(setReservation(result))
            })
            .catch((e) => {
                //console
            })
    }
}