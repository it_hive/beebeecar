import config from './../../../config';
import {getJson} from "../../../utils/http";
import {SET_NEW_USER, setNewUser} from "./user";
import fetchUsers from "./user";
import {getUserInfos, getUtilisateurToken} from "../reducers/connexion";

export const SET_LIST_SITE_ORGA = "SET_LIST_SITE_ORGA";
export const SET_NEW_SITE = "SET_NEW_SITE";

export const setListSiteOrga = (donnee) => {
    return {type: SET_LIST_SITE_ORGA, donnee}
}

export const setNewSite = (donnee) => {
    return {type: SET_NEW_SITE, donnee}
}

export function fetchListSiteOrga(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'sites',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            }
        }).then(response => getJson(response))
        .then(result => {
            dispatch(setListSiteOrga(result))
        })
        .catch((e) => {
            //console
        })
    }
}

export function saveSite(input){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'site/new',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                'adresse' : input.adress,
                'codePostal' : input.codePostal,
                'nom' : input.name,
                'orgId' : getUserInfos(getState()).idOrganisme,
                'ville' : input.city
            })
        })
        .then(response => {
            dispatch(setNewSite(response.status))
            dispatch(fetchListSiteOrga())
        })
        .catch((e) => {
            //console
        })
    }
}

export function updateSite(input, idSite){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'site/' + idSite,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                'adresse' : input.adresse,
                'codePostal' : input.codePostal,
                'nom' : input.nom,
                'orgId' : getUserInfos(getState()).idOrganisme,
                'ville' : input.ville
            })
        })
        .then(response => {
            dispatch(setNewSite(response.status))
            dispatch(fetchListSiteOrga())
        })
        .catch((e) => {
            //console
        })
    }
}