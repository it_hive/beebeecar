import config from "../../../config";
import {getJson} from "../../../utils/http";
import dateFns from "date-fns";
var fr = require('date-fns/locale/fr'); // Import french translation for date

export const SET_CHART = "SET_CHART";

export const TAUX_OCCUPATION_SITE = "TAUX_OCCUPATION_SITE";
export const TAUX_OCCUPATION_VEHICULE = "TAUX_OCCUPATION_VEHICULE";
export const ETAT_PARC = "ETAT_PARC";
export const UTILISATEUR_ACTIF = "UTILISATEUR_ACTIF";
export const NB_RESERVATION_ETAT = "NB_RESERVATION_ETAT";

export const setActiveChart = (donnee) => {
    return { type: SET_CHART, donnee}
};

export const setTauxOccupationSite = (donnee) => {
  return { type: TAUX_OCCUPATION_SITE, donnee}
};

export const setTauxOccupationVehicule = (donnee) => {
    return { type: TAUX_OCCUPATION_VEHICULE, donnee}
};

export const setEtatParc = (donnee) => {
    return { type: ETAT_PARC, donnee}
};

export const setUtilisateurActif = (donnee) => {
    return { type: UTILISATEUR_ACTIF, donnee }
};

export const setNbReservationEtat = (donnee) => {
    return {type: NB_RESERVATION_ETAT, donnee}
};

export function fetchTauxOccupationSite(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`statistiques/site/tauxoccupation/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "dateReference": dateFns.format(Date.now(), "DD/MM/YYYY", {locale: fr})
            })
        })
        .then(response => getJson(response))
        .then(result => {
            dispatch(setTauxOccupationSite(result));
        })
        .catch((e) => {
        })
    }
}

export function fetchTauxOccupationVehicule(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`statistiques/vehicule/tauxoccupation/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "dateReference": dateFns.format(Date.now(), "DD/MM/YYYY", {locale: fr})
            })
        })
        .then(response => getJson(response))
        .then(result => {
            dispatch(setTauxOccupationVehicule(result));
        })
        .catch((e) => {
        })
    }
}

export function fetchEtatParc(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`statistiques/etatParc/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "dateReference": dateFns.format(Date.now(), "DD/MM/YYYY", {locale: fr})
            })
        })
        .then(response => getJson(response))
        .then(result => {
            dispatch(setEtatParc(result));
        })
        .catch((e) => {
        })
    }
}

export function fetchUtilisateurActif(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`statistiques/utilisateurs/nombreReservations/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "dateReference": dateFns.format(Date.now(), "DD/MM/YYYY", {locale: fr})
            })
        })
            .then(response => getJson(response))
            .then(result => {
                dispatch(setUtilisateurActif(result));
            })
            .catch((e) => {
            })
    }
}

export function fetchNbReservationEtat(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`statistiques/site/nombreReservationsByEtat/${getState().auth.token.token}`,{
            method: 'POST',
            headers: {
                'Accept' : '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "dateReference": dateFns.format(Date.now(), "DD/MM/YYYY", {locale: fr})
            })
        })
            .then(response => getJson(response))
            .then(result => {
                const data = [];
                result.map((item, i) => {
                    if(data.find(o =>  o.id.toString() === item.sitId.toString()) === undefined){
                        data.push({
                            id: item.sitId,
                            nom: item.sitNom,
                            EnCoursDeValidation: [],
                            Valide: [],
                            Refuse: [],
                            Annule: [],
                            AnnuleCarArchivage: []
                        })
                    }

                    data[data.findIndex(o => o.id.toString() === item.sitId.toString())][item.edmLibelle] = [
                        item.demandesN11,
                        item.demandesN10,
                        item.demandesN9,
                        item.demandesN8,
                        item.demandesN7,
                        item.demandesN6,
                        item.demandesN5,
                        item.demandesN4,
                        item.demandesN3,
                        item.demandesN2,
                        item.demandesN1,
                        item.demandesN
                    ];
                })

                dispatch(setNbReservationEtat(data));
            })
            .catch((e) => {
            })
    }
}
