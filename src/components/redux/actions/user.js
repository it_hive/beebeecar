import config from './../../../config';
import {getJson} from './../../../utils/http';
import {getUserInfos} from './../reducers/connexion';
import {getUtilisateurToken} from "../reducers/connexion";
import {fetchInfosUtilisateur} from "./connexion";

export const SET_LIST_USER = "SET_LIST_USER";
export const SET_NEW_USER = "SET_NEW_USER";
export const SET_ARCHIVE_USER = "SET_ARCHIVE_USER";
export const SET_SHOW_TUTO = "SET_SHOW_TUTO";
export const SET_EDIT_PASSWORD = "SET_EDIT_PASSWORD";

export const setUsers = (result) => {
    return { type: SET_LIST_USER, result}
};

export const setNewUser = (result) => {
    return {type: SET_NEW_USER, result}
};

export const setArchiveUser = (result) => {
    return { type: SET_ARCHIVE_USER, result}
};

export const setShowTuto = (donnee) => {
  return {type: SET_SHOW_TUTO, donnee}
};

export const setEditPassword = (donnee) => {
    return {type: SET_EDIT_PASSWORD, donnee};
};

export default function fetchUsers(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'utilisateurs',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            }
        })
        .then(response => getJson(response))
        .then(response => {
            dispatch(setUsers(response))
        })
        .catch((e) => {
            //console
        })
    }
}

export function saveUser(input){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'utilisateur/new',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: bodyUser(getState, input)
        })
        .then(response => {
            const statusCode = response.status;
            const data = response.json();
            return Promise.all([statusCode, data]);
        })
        .then(response => {
            dispatch(setNewUser(response[0]));
            dispatch(sendUserEmail(response[1].idUtilisateur));
            dispatch(fetchUsers())
        })
        .catch((e) => {
            //console
        })
    }
}

export function sendUserEmail(idUtilisateur){
    return (dispatch, getState) => {
        fetch(config['base_url'] +`utilisateur/email/${idUtilisateur}`,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then()
        .catch()
    }
}

export function updateUser(input, idUser){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'utilisateur/' + idUser,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: bodyUser(getState, input, idUser)
        }).then(response => {
            dispatch(setNewUser(response.status));
            dispatch(fetchUsers());
            dispatch(fetchInfosUtilisateur(getUtilisateurToken(getState()).token))
        })
        .catch((e) => {
            //console
        })
    }
}

function bodyUser(getState, input, idUser){
    return JSON.stringify({
        'admin' : input.isAdmin,
        'hasPermis' : input.asPermis,
        'idOrganisme' : getUserInfos(getState()).idOrganisme,
        'idUtilisateur' : idUser ? idUser : getUserInfos(getState()).idUtilisateur,
        'mail' : input.mail,
        'nom' : input.name,
        'nomOrganisme' : getUserInfos(getState()).nomOrganisme,
        'prenom' : input.surname,
        'telephone' : input.phone,
        'photo' : '',
        'motPasse': input.password
    })
}

export function archiveUser(idUser){
    return(dispatch, getState) => {
        fetch(config['base_url'] +'utilisateur/archivage/' + idUser,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            }
        }).then(response => {
            dispatch(setArchiveUser(response.status));
            dispatch(fetchUsers());
        })
        .catch((e) => {
            //console
        })
    }
}

export function updatePassword(newPassword, mail, idUser){
    return(dispatch, getState) => {
        fetch(config['base_url'] +'utilisateur/paswword/' + idUser,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                'mailUtilisateur' : mail,
                'motDePasse' : newPassword
            })
        }).then(response => {
            dispatch(setEditPassword(response.status));
        })
        .catch((e) => {
            //console
        })
    }
}