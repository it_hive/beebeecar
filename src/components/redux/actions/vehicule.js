import config from './../../../config';
import {getJson} from "../../../utils/http";
import {getUserInfos, getUtilisateurToken} from "../reducers/connexion";
import {dateDuJour, formatDateToString} from "../../../utils/date";

export const SET_LIST_VEH = "SET_LIST_VEH";
export const SET_LIST_MARQUE = "SET_LIST_MARQUE";
export const SET_LIST_MODELE = "SET_LIST_MODELE";
export const SET_NEW_VEH = "SET_NEW_VEH";
export const SET_SUPPR_VEH = "SET_SUPPR_VEH";

export const setVehicules = (result) => {
    return { type: SET_LIST_VEH, result}
};

export const setMarques = (donnee) => {
    return { type: SET_LIST_MARQUE, donnee}
};

export const setModele = (donnee) => {
    return {type: SET_LIST_MODELE, donnee}
};

export const setNewVeh = (donnee) => {
    return {type: SET_NEW_VEH, donnee}
};

export const setSupprVeh = (donnee) => {
    return {type: SET_SUPPR_VEH, donnee}
};

export default function fetchVehicules(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'voitures/org/' + getUserInfos(getState()).idOrganisme,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'token': getState().auth.token.token
            }
        }).then(response => getJson(response))
        .then(result => {
            dispatch(setVehicules(result))
        })
        .catch((e) => {
            //console
        })
    }
}

export function fetchMarques(){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'marques',{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            }
        }).then(response => getJson(response))
            .then(result => {
                dispatch(setMarques(result))
            })
            .catch((e) => {
                //console
            })
    }
}

export function fetchModele(marqueId){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'modeles/' + marqueId,{
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            }
        }).then(response => getJson(response))
            .then(result => {
                dispatch(setModele(result))
            })
            .catch((e) => {
                //console
            })
    }
}

export function fetchCreateVeh(input){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'voiture/new',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                'couleur': input.color,
                'dateFinService' : '',
                'dateMiseEnService' : formatDateToString(input.dateMiseEnService),
                'details' : (input.details?input.details:''),
                'idMarque' : input.marque,
                'idModele' : input.idModele,
                'idSite' : input.idSite,
                'immatriculation' : input.siv,
                'photo' : ''
            })
        })
        .then(response => {
            dispatch(setNewVeh(response.status));
            dispatch(fetchVehicules());
        })
        .catch((e) => {
            //console
        })
    }
}


export function fetchUpdateVeh(input, idVeh){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'voiture/' + idVeh,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                'id_vehicule': input.id_vehicule,
                'dateMiseEnService' : input.dateMiseEnService,
                'idMarque' : input.idMarque,
                'idModele' : input.idModele,
                'idSite' : input.idSite,
                'immatriculation' : input.immatriculation,
                'kilometrage' : parseInt(input.kilometrage),
                'details' : input.details,
                'dateFinService' : input.dateFinService
            })
        })
        .then(response => {
            dispatch(setNewVeh(response.status));
            dispatch(fetchVehicules());
        })
        .catch((e) => {
            //console
        })
    }
}

export function deleteVeh(idVeh, dateFin){
    return (dispatch, getState) => {
        fetch(config['base_url'] +'voiture/archivage/' + idVeh,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'token': getUtilisateurToken(getState()).token
            },
            body: JSON.stringify({
                dateFindeService : formatDateToString(dateFin)
            })
        })
        .then(response => {
            dispatch(setSupprVeh(response.status))
            dispatch(fetchVehicules())
        })
        .catch((e) => {
            //console
        })
    }
}
