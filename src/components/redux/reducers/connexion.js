import { SET_AUTH, SET_DISCONNECT, SET_CODE_RETOUR, SET_USER_INFO } from './../actions/connexion.js';
import {SET_AUTH_ERROR} from "./../actions/connexion";
import { REHYDRATE } from 'redux-persist';

const initialState = {
    isAuthenticate : false,
    connexionData : {},
    token : null,
    retour : null,
    msgError: ""
};

export default function(state = initialState, action) {
    switch(action.type) {
        case REHYDRATE:
            return {
                ...state,
                msgError: ""
            };
        case SET_AUTH :
            return {...state, token : action.donnee};
        case SET_DISCONNECT:
            return {};
        case SET_CODE_RETOUR:
            return {...state, retour : action.donnee};
        case SET_USER_INFO:
            return {...state, isAuthenticate : true,  connexionData : action.donnee, msgError: ""};
        case SET_AUTH_ERROR:
            return {...state, msgError: action.donnee};
        default :
            return state;
    }
}
export function getUtilisateurToken(state){
    return state.auth.token;
}

export function isAuthenticate(state) {
    return state.auth.isAuthenticate;
}

export function isAdmin(state){
    if(state.auth.connexionData)
        return state.auth.connexionData.admin;
    else
        return false;
}

export function getUserInfos(state){
    return state.auth.connexionData;
}

export function getFirstAndUsername(state){
    let nom = state.auth.connexionData.nom;
    let prenom = state.auth.connexionData.prenom;

    return prenom.charAt(0).toUpperCase() + prenom.slice(1) + " " + nom.charAt(0).toUpperCase() + nom.slice(1);
}

export function getAuthError(state){
    return state.auth.msgError;
}