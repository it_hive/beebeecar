import {
    SET_DEMANDE_REFUSE,
    SET_DEMANDE_VALIDATE,
    SET_DEMANDES_JOUR,
    SET_DEMANDES_LOCATIONS,
    SET_DEMANDES_LOCATIONS_DETAILS
} from "../actions/demandeLocation";

const initialState = {
    listDemandes: [],
    detailDemande: {},
    demandeJour: [],
    isValidate : null,
    isRefuse : null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_DEMANDES_LOCATIONS :
            return {...state, listDemandes : action.result};
        case SET_DEMANDES_LOCATIONS_DETAILS :
            return {...state, detailDemande : action.result};
        case SET_DEMANDES_JOUR :
            return {...state, demandeJour : action.donnee};
        case SET_DEMANDE_VALIDATE:
            return {...state, isValidate : action.donnee}
        case SET_DEMANDE_REFUSE:
            return {...state, isRefuse : action.donnee}
        default :
            return state;
    }
}

export function getDemandeLocation(state) {
    return state.demandeLocation.listDemandes;
}

export function getDemandeLocationDetails(state){
    return state.demandeLocation.detailDemande;
}

export function getDemandesJour(state){
    return state.demandeLocation.demandeJour;
}

export function demandeIsValidate(state){
    return state.demandeLocation.isValidate;
}

export function demandeIsRefuse(state){
    return state.demandeLocation.isRefuse;
}