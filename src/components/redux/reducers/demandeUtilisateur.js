const initialState = {
    demandeAttente: [],
    demande: [],
    vehiculeDispo: [],
    isCreate: null,
    listeCovoit: []
};

export default function(state = initialState, action) {
    switch(action.type) {
        case 'SET_DEMANDE_UTILISATEUR_ATTENTE':
            return {...state, demandeAttente : action.donnee};
        case 'SET_DEMANDE_UTILISATEUR':
            return {...state, demande : action.donnee};
        case 'SET_DEMANDE_VEHICULE_DISPO':
            return {...state, vehiculeDispo: action.donnee};
        case 'SET_DEMANDE_IS_CREATE':
            return {...state, isCreate: action.donnee};
        case 'SET_LISTE_COVOIT':
            return {...state, listeCovoit: action.donnee};
        default :
            return state;
    }
}

export function getDemandeUtilisateurAttente(state){
    return state.demandeUtilisateur.demandeAttente;
}

export function getDemandeUtilisateur(state){
    return state.demandeUtilisateur.demande;
}

export function getDemandeVehiculeDispo(state){
    return state.demandeUtilisateur.vehiculeDispo;
}

export function getDemandeIsCreate(state){
    return state.demandeUtilisateur.isCreate;
}

export function getListeCovoit(state){
    return state.demandeUtilisateur.listeCovoit;
}