import { SET_MENU_CHOICE } from './../actions/menu.js';
import { SET_OPEN_MENU } from './../actions/menu.js';
import { SET_OPEN_MENU_DASHBOARD } from './../actions/menu.js';
import {REHYDRATE} from "redux-persist/es/constants";

const initialState = {
    menuChoice : "dashboard",
    openMenu: false,
    openMenuDashboard: true
}

export default function(state = initialState, action) {
    switch(action.type) {
        case REHYDRATE:
            return {
                ...state,
                menuChoice: "dashboard"
            };
        case SET_MENU_CHOICE :
            return {...state, menuChoice : action.donnee}
        case SET_OPEN_MENU :
            return {...state, openMenu : action.donnee}
        case SET_OPEN_MENU_DASHBOARD :
            return {...state, openMenuDashboard : action.donnee}
        default :
            return state;
    }
}

export function getMenuChoice(state) {
    return state.menu.menuChoice;
}

export function getOpenMenu(state){
    return state.menu.openMenu;
}

export function getOpenMenuDashboard(state){
    return state.menu.openMenuDashboard;
}