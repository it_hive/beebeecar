import {SET_LIST_RESERVATION} from "../actions/reservation";

const initialState = {
    listReservation: []
};

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_LIST_RESERVATION :
            return {...state, listReservation : action.result}
        default :
            return state;
    }
}

export function getReservations(state) {
    return state.reservation.listReservation;
}