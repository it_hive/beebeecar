import {SET_LIST_SITE_ORGA, SET_NEW_SITE} from "./../actions/site"

const initialState = {
    listSiteOrga : [],
    isCreate: null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_LIST_SITE_ORGA :
            return {...state, listSiteOrga : action.donnee}
        case SET_NEW_SITE :
            return {...state, isCreate : action.donnee}
        default :
            return state;
    }
}

export function getListSiteOrga(state) {
    return state.site.listSiteOrga;
}

export function getIsCreate(state) {
    return state.site.isCreate;
}