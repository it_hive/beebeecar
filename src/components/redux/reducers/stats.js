import { SET_CHART } from './../actions/stats.js';
import {NB_RESERVATION_ETAT, TAUX_OCCUPATION_SITE, TAUX_OCCUPATION_VEHICULE, UTILISATEUR_ACTIF} from "../actions/stats";
import {ETAT_PARC} from "./../actions/stats";

const initialState = {
    activeChart : "taux_occupation",
    tauxOccupationSite: [],
    tauxOccupationVehicule: [],
    etatParc: [],
    utilisateurActif: [],
    nbReservationEtat: []
};

export default function(state = initialState, action) {
    switch(action.type) {
        case SET_CHART :
            return Object.assign({}, state, {
                activeChart: action.donnee
            });
        case TAUX_OCCUPATION_SITE:
            return { ...state, tauxOccupationSite: action.donnee };
        case TAUX_OCCUPATION_VEHICULE:
            return { ...state, tauxOccupationVehicule: action.donnee };
        case ETAT_PARC:
            return { ...state, etatParc: action.donnee };
        case UTILISATEUR_ACTIF:
            return { ...state, utilisateurActif: action.donnee };
        case NB_RESERVATION_ETAT:
            return { ...state, nbReservationEtat: action.donnee };
        default :
            return state;
    }
}

export function getActiveChart(state) {
    return state.stats.activeChart;
}

export function getTauxOccupationSite(state){
    return state.stats.tauxOccupationSite;
}

export function getTauxOccupationVehicule(state){
    return state.stats.tauxOccupationVehicule;
}

export function getEtatParc(state){
    return state.stats.etatParc;
}

export function getUtilisateurActif(state){
    return state.stats.utilisateurActif;
}

export function getNbReservationEtat(state){
    return state.stats.nbReservationEtat;
}