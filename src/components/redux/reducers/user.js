import {SET_LIST_USER, SET_NEW_USER, SET_ARCHIVE_USER, SET_SHOW_TUTO, SET_EDIT_PASSWORD} from "./../actions/user"

const initialState = {
    listUser : [],
    isCreate : null,
    isArchive : null,
    showTuto: true,
    isEditPassword: null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_LIST_USER :
            return {...state, listUser : action.result};
        case SET_NEW_USER :
            return {...state, isCreate : action.result};
        case SET_ARCHIVE_USER:
            return {...state, isArchive : action.result};
        case SET_SHOW_TUTO:
            return {...state, showTuto : action.donnee};
        case SET_EDIT_PASSWORD:
            return {...state, isEditPassword : action.donnee};
        default :
            return state;
    }
};

export function getIsCreate(state){
    return state.user.isCreate;
}

export function getIsArchive(state){
    return state.user.isArchive;
}

export function getListUser(state){
    return state.user.listUser;
}

export function getMdpEdit(state){
    return state.user.isEditPassword;
}

export function getNBuser(state) {
    let i = 0;
    state.user.listUser && state.user.listUser.forEach(item => {
        if(!item.archived){
            i++;
        }
    });
    return i;
};

export function getShowTuto(state){
    return state.user.showTuto;
};