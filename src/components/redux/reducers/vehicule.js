import {SET_LIST_VEH, SET_LIST_MARQUE, SET_LIST_MODELE, SET_NEW_VEH, SET_SUPPR_VEH} from "./../actions/vehicule"

const initialState = {
    listVeh : [],
    listMarque : [],
    listModele : [],
    isCreate : null,
    isSuppr : null
};

export default (state = initialState, action) => {
    switch(action.type) {
        case SET_LIST_VEH :
            return {...state, listVeh : action.result}
        case SET_LIST_MARQUE :
            return {...state, listMarque : action.donnee}
        case SET_LIST_MODELE:
            return {...state, listModele : action.donnee}
        case SET_NEW_VEH:
            return {...state, isCreate : action.donnee}
        case SET_SUPPR_VEH:
            return {...state, isSuppr : action.donnee}
        default :
            return state;
    }
}

export function getMarques(state) {
    return state.vehicule.listMarque;
}

export function getModeles(state) {
    return state.vehicule.listModele;
}

export function getIsCreate(state){
    return state.vehicule.isCreate;
}

export function getListVeh(state) {
    return state.vehicule.listVeh;
}

export function getIsSuppr(state){
    return state.vehicule.isSuppr;
}

export function getVehicule(state){
    return state.vehicule.listVeh;
}