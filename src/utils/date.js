/**
 * Format date from server to return dd/MM/YYYY
 * 2019-05-09T09:58:16.000+0000
 */
export function formatDateServer(date){
    let formattedDate = null;

    formattedDate = date.substring(0, date.indexOf("T"));
    formattedDate = formattedDate.split("-");

    return new Date(formattedDate[0], parseInt(formattedDate[1])-1, formattedDate[2]);
}

export function  formatDateToString(laDate) {
    let formattedDate = laDate.split("-");
    return `${formattedDate[2]}/${formattedDate[1]}/${formattedDate[0]}`

}

export function dateDuJour(){
    let now = new Date();

    let annee   = now.getFullYear();
    let mois  = now.getMonth()+1
    mois    = ('0'+mois).slice(-2);
    let jour    = ('0'+now.getDate()   ).slice(-2);
    return jour + "/" + mois + "/" + annee;
}